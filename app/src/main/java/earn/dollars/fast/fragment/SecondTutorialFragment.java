package earn.dollars.fast.fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.activity.TutorialActivity;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.rest.body.ApplyRefCodeBody;
import earn.dollars.fast.rest.response.ApplyRefCodeResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecondTutorialFragment extends BaseTutorialFragment {

    @BindView(R.id.tvRefCodeDescription)
    protected TextView tvRefCodeDescription;

    @BindView(R.id.etRefCode)
    protected EditText etRefCode;

    @BindView(R.id.tvApply)
    protected TextView tvApply;

    @BindView(R.id.llContent)
    protected LinearLayout llContent;

    @BindView(R.id.ivArrowBack)
    protected ImageView arrowOne;

    @BindView(R.id.ivArrowForvard)
    protected ImageView arrowTwo;

    @BindView(R.id.pager3)
    protected ImageView pager3;


    public static SecondTutorialFragment newInstance(TutorialActivity activity) {
        SecondTutorialFragment fragment = new SecondTutorialFragment();
        fragment.activity = activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_tutorial, container, false);
        unbinder = ButterKnife.bind(this, view);
        initViews();
        Utility.setDrawableColor(arrowOne, App.getContext().getResources().getColor(R.color.orange));
        Utility.setDrawableColor(arrowTwo, App.getContext().getResources().getColor(R.color.orange));
        Utility.setDrawableColor(pager3, App.getContext().getResources().getColor(R.color.orange));
        return view;
    }

    private void initViews() {
        tvRefCodeDescription.setText(getActivity()
                .getResources()
                .getString(R.string.enter_ref_code_descr, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency())
        );

        etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
        etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());

        if (!App.getCurrentUser().getMyReferrer().isEmpty()) {
            etRefCode.setText(App.getCurrentUser().getMyReferrer());
        }
    }

    @OnClick(R.id.tvApply)
    protected void onApplyRefCod() {
        if (App.getCurrentUser().getRefCode().isEmpty()) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.gen_ref_code), Toast.LENGTH_SHORT).show();
            return;
        } else if (App.getCurrentUser().getRefCode().length() != etRefCode.getText().toString().length()) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.ref_code_not_find), Toast.LENGTH_SHORT).show();
            return;
        }else if (App.getCurrentUser().getRefCode().equals(etRefCode.getText().toString())){
            Toast.makeText(getActivity(), getResources().getString(R.string.its_your_ref_code), Toast.LENGTH_SHORT).show();
            return;
        }else{
            App.getCurrentUser().setRefCodeApplied(true);
            App.getCurrentUser().increaseBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
            App.getCurrentUser().increaseRefBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
            App.getCurrentUser().setMyReferrer(etRefCode.getText().toString());
            App.getCurrentUser().save();

            etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
            etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());


            Toast.makeText(App.getContext(), App.getContext().getResources().getString(R.string.ref_code_successful, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency()), Toast.LENGTH_SHORT).show();
        }



//        Call<ApplyRefCodeResponse> call = App.getCraneApi().applyRefCode(new ApplyRefCodeBody(App.getCurrentUser().getRefCode(), etRefCode.getText().toString()));
//
//        call.enqueue(new Callback<ApplyRefCodeResponse>() {
//            @Override
//            public void onResponse(Call<ApplyRefCodeResponse> call, Response<ApplyRefCodeResponse> response) {
//                ApplyRefCodeResponse resp = response.body();
//                if (resp.getResult()) {
//                    App.getCurrentUser().setRefCodeApplied(true);
//                    App.getCurrentUser().increaseBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
//                    App.getCurrentUser().increaseRefBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
//                    App.getCurrentUser().setMyReferrer(etRefCode.getText().toString());
//                    App.getCurrentUser().save();
//
//                    etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
//                    etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());
//
//                    tvApply.setClickable(!App.getCurrentUser().isRefCodeApplied());
//                    tvApply.setEnabled(!App.getCurrentUser().isRefCodeApplied());
//
//                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.ref_code_successful, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency()), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ApplyRefCodeResponse> call, Throwable t) {
//                Snackbar snackbar = Snackbar
//                        .make(llContent, R.string.error_connect, Snackbar.LENGTH_LONG);
//                snackbar.show();
//            }
//        });
    }
}

