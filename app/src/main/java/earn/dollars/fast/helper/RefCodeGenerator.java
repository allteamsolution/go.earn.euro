package earn.dollars.fast.helper;

import java.util.Random;

import code.junker.JunkProvider;


public class RefCodeGenerator {

    public static final int REF_CODE_LEN = 8;
    public static final int USER_ID_LEN = 12;
    public static final int TXS_LEN = 25;


    public static String generate(){
        JunkProvider.f();

        int length = REF_CODE_LEN;
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }
        return result.toString().toUpperCase();
    }

    public static String generateUserId(){
        JunkProvider.f();

        int length = USER_ID_LEN;
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {

            if (length == 4 || length == 8 || length == 12 || length == 16){
                result.append('-');
                length--;
                continue;
            }
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }

        return result.toString().toUpperCase();
    }

    public static String generateTxs(){
        JunkProvider.f();

        int length = TXS_LEN;
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }

        return result.toString().toUpperCase();
    }
}
