package earn.dollars.fast.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.activity.MainActivity;
import earn.dollars.fast.dialog.GreatMinerBuyDialogHelper;
import earn.dollars.fast.event.MineStartedEvent;
import earn.dollars.fast.event.MineStoppedEvent;
import earn.dollars.fast.event.MinedEvent;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.ShopHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.model.User;
import earn.dollars.fast.receiver.NetworkStateReceiver;
import earn.dollars.fast.service.MiningForegroundService;
import fx.AwsApp;
import fx.helper.AwsApiHelper;
import fx.helper.AwsAppPreferenceManager;
import fx.helper.loco.LocoMotif;


public class MainFragment extends Fragment implements NetworkStateReceiver.NetworkStateReceiverListener {

    @BindView(R.id.maningImageView)
    protected ImageView ivStartStop;

    @BindView(R.id.ivAnimated)
    protected ImageView ivAnimated;

    @BindView(R.id.tvBalanceMainFragment)
    protected TextView tvBalanceMainFragment;

    @BindView(R.id.offers)
    protected ImageView offers;

//    @BindView(R.id.powerMainFragment)
//    protected TextView powerMainFragment;
//
//    @BindView(R.id.minerOne)
//    protected ImageView minerOne;
//
//    @BindView(R.id.minerTwo)
//    protected ImageView minerTwo;
//
//    @BindView(R.id.minerThree)
//    protected ImageView minerThree;
//
//    @BindView(R.id.minerFour)
//    protected ImageView minerFour;
//
//    @BindView(R.id.minerFive)
//    protected ImageView minerFive;
//
//    @BindView(R.id.minerSix)
//    protected ImageView minerSix;
//
//    @BindView(R.id.powerMinerOne)
//    protected TextView powerMinerOne;
//
//    @BindView(R.id.powerMinerTwo)
//    protected TextView powerMinerTwo;
//
//    @BindView(R.id.powerMinerThree)
//    protected TextView powerMinerThree;
//
//    @BindView(R.id.powerMinerFour)
//    protected TextView powerMinerFour;
//
//    @BindView(R.id.powerMinerFive)
//    protected TextView powerMinerFive;
//
//    @BindView(R.id.powerMinerSix)
//    protected TextView powerMinerSix;

    private NetworkStateReceiver networkStateReceiver;
    private MiningForegroundService mService;
    private boolean mBound = false;
    private boolean isAnimateStart ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        ///updateBalance();
//        inAppNotificationControl = new InAppNotificationControl(llNotif);
//        initPayout();
        init();
        initGreatMiners();
        checkOffersTime();
        return view;
    }


    private void init() {

        AwsApiHelper.checkUpdate();
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        getContext().registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        //setNoInetWidgetVisible(!Utility.hasInternet(this));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MinedEvent event) {
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStartedEvent event) {
        initStartBtn(true);
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStoppedEvent event) {
        initStartBtn(false);
        updateBalance();
    }


    private void updateBalance() {

        double curentBalance = App.getCurrentUser().getBalance();
        tvBalanceMainFragment.setText(Utility.formatBalance(curentBalance) + " " + Constants.CRYPT_CODE);
        // powerMainFragment.setText(getString(R.string.power) + " " +  App.getCurrentUser().getPower() + " GH/s");
    }

    private void updatePower() {
        //powerMainFragment.setText(getString(R.string.power) + " " + String.format("%.2f", App.getCurrentUser().getPower()) + " GH/s");
    }


    @Override
    public void onResume() {
        super.onResume();

        updateBalance();
        initGreatMiners();
        App.getUIHandler().post(timerRunnable);
    }

    private void initStartBtn(boolean isServiceRunning) {
        if (isServiceRunning) {
            updateBalance();
            if (NetworkStateReceiver.connected) {
                ((AnimationDrawable) ivAnimated.getDrawable()).start();
                ivStartStop.setImageResource(R.drawable.pause_button);
                isAnimateStart = true;
            }else{
                Toast.makeText(getContext(),getResources().getString(R.string.no_inet_service), Toast.LENGTH_SHORT).show();
            }

        } else {
            ivStartStop.setImageResource(R.drawable.power_on_button);
            updateBalance();
            isAnimateStart = false;

            ((AnimationDrawable) ivAnimated.getDrawable()).stop();
        }

        updateBalance();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        // Bind to LocalService
        Intent intent = new Intent(getContext(), MiningForegroundService.class);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        if (mService != null) {
            initStartBtn(mService.isRunning());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        getActivity().unbindService(mConnection);
        mBound = false;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MiningForegroundService.LocalBinder binder = (MiningForegroundService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            initStartBtn(mService.isRunning());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;

        }
    };

    @OnClick(R.id.maningImageView)
    public void onivStartStopClicked() {
        if (mService != null) {
            if (mService.isRunning()) {
                Intent service = MiningForegroundService.createIntent(MiningForegroundService.ACTION_STOP_MINING);
                App.getContext().startService(service);
            } else {
                Log.d("JJJJJJJJ", "save time : " + Utility.getTime());
                App.getCurrentUser().timeLastRestart = Utility.getTime();
                App.setCurrentUser(App.getCurrentUser());
                Intent service = MiningForegroundService.createIntent(MiningForegroundService.ACTION_START_MINING);
                App.getContext().startService(service);
            }
        } else {
        }
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

    public void initGreatMiners() {
        int iMinerOne = App.getCurrentUser().getGreatMinerIdOne();
        int iMinerTwo = App.getCurrentUser().getGreatMinerIdTwo();
        int iMinerThree = App.getCurrentUser().getGreatMinerIdThree();
        int iMinerFour = App.getCurrentUser().getGreatMinerIdFour();
        int iMinerFive = App.getCurrentUser().getGreatMinerIdFive();
        int iMinerSix = App.getCurrentUser().getGreatMinerIdSix();

        List<Integer> list = new ArrayList<>();
        list.add(iMinerOne);
        list.add(iMinerTwo);
        list.add(iMinerThree);
        list.add(iMinerFour);
        list.add(iMinerFive);
        list.add(iMinerSix);

        float powerBalance = ShopHelper.getGreatMinerPowerToTextView(list);
        App.getCurrentUser().setPower(powerBalance);
        updateBalance();

//        if (iMinerOne == 0) {
//            minerOne.setImageResource(R.drawable.videcard_passive);
//            powerMinerOne.setText(ShopHelper.getGreatMinerPower(iMinerOne));
//        } else {
//            minerOne.setImageResource(R.drawable.videcard_active);
//            powerMinerOne.setText(ShopHelper.getGreatMinerPower(iMinerOne));
//        }
//
//        if (iMinerTwo == 0) {
//            minerTwo.setImageResource(R.drawable.videcard_passive);
//            powerMinerTwo.setText(ShopHelper.getGreatMinerPower(iMinerTwo));
//        } else {
//            minerTwo.setImageResource(R.drawable.videcard_active);
//            powerMinerTwo.setText(ShopHelper.getGreatMinerPower(iMinerTwo));
//        }
//
//        if (iMinerThree == 0) {
//            minerThree.setImageResource(R.drawable.videcard_passive);
//            powerMinerThree.setText(ShopHelper.getGreatMinerPower(iMinerThree));
//        } else {
//            minerThree.setImageResource(R.drawable.videcard_active);
//            powerMinerThree.setText(ShopHelper.getGreatMinerPower(iMinerThree));
//        }
//
//        if (iMinerFour == 0) {
//            minerFour.setImageResource(R.drawable.videcard_passive);
//            powerMinerFour.setText(ShopHelper.getGreatMinerPower(iMinerFour));
//        } else {
//            minerFour.setImageResource(R.drawable.videcard_active);
//            powerMinerFour.setText(ShopHelper.getGreatMinerPower(iMinerFour));
//        }
//
//        if (iMinerFive == 0) {
//            minerFive.setImageResource(R.drawable.videcard_passive);
//            powerMinerFive.setText(ShopHelper.getGreatMinerPower(iMinerFive));
//        } else {
//            minerFive.setImageResource(R.drawable.videcard_active);
//            powerMinerFive.setText(ShopHelper.getGreatMinerPower(iMinerFive));
//        }
//
//        if (iMinerSix == 0) {
//            minerSix.setImageResource(R.drawable.videcard_passive);
//            powerMinerSix.setText(ShopHelper.getGreatMinerPower(iMinerSix));
//        } else {
//            minerSix.setImageResource(R.drawable.videcard_active);
//            powerMinerSix.setText(ShopHelper.getGreatMinerPower(iMinerSix));
//        }

    }

//    @OnClick(R.id.minerOne)
//    public void onMinerOneClicked() {
//        if (App.getCurrentUser().getGreatMinerIdOne() != 5) {
//
//
//            GreatMinerBuyDialogHelper.show(getContext(), new GreatMinerBuyDialogHelper.GreatMinerBuyDialogHelperClickListener() {
//
//                @Override
//                public void onBuyGreatMinerClicked(int amount) {
////                    initGreatMiners();
////                    updateBalance();
//                }
//            }, App.getCurrentUser().getGreatMinerIdOne(),1);
//        } else {
//            Toast toast = Toast.makeText(getActivity(),
//                    getString(R.string.full_upgrade), Toast.LENGTH_SHORT);
//            toast.show();
//        }
//
//
//    }
//
//    @OnClick(R.id.minerTwo)
//    public void onMinerTwoClicked() {
//        if (App.getCurrentUser().getGreatMinerIdTwo() != 5) {
//            GreatMinerBuyDialogHelper.show(getContext(), new GreatMinerBuyDialogHelper.GreatMinerBuyDialogHelperClickListener() {
//
//                @Override
//                public void onBuyGreatMinerClicked(int amount) {
//
//                }
//            }, App.getCurrentUser().getGreatMinerIdTwo(),2);
//        } else {
//            Toast toast = Toast.makeText(getActivity(),
//                    getString(R.string.full_upgrade), Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }
//
//    @OnClick(R.id.minerThree)
//    public void onMinerThreeClicked() {
//        if (App.getCurrentUser().getGreatMinerIdThree() != 5) {
//            GreatMinerBuyDialogHelper.show(getActivity(), new GreatMinerBuyDialogHelper.GreatMinerBuyDialogHelperClickListener() {
//
//                @Override
//                public void onBuyGreatMinerClicked(int amount) {
//
//                }
//            }, App.getCurrentUser().getGreatMinerIdThree(),3);
//        }
//    }
//
//    @OnClick(R.id.minerFour)
//    public void onMinerFourClicked() {
//        if (App.getCurrentUser().getGreatMinerIdFour() != 5) {
//            GreatMinerBuyDialogHelper.show(getContext(), new GreatMinerBuyDialogHelper.GreatMinerBuyDialogHelperClickListener() {
//
//                @Override
//                public void onBuyGreatMinerClicked(int amount) {
//
//                }
//            }, App.getCurrentUser().getGreatMinerIdFour(),4);
//        } else {
//            Toast toast = Toast.makeText(getActivity(),
//                    getString(R.string.full_upgrade), Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }
//
//    @OnClick(R.id.minerFive)
//    public void onMinerFiveClicked() {
//
//        if (App.getCurrentUser().getGreatMinerIdFive() != 5) {
//            GreatMinerBuyDialogHelper.show(getContext(), new GreatMinerBuyDialogHelper.GreatMinerBuyDialogHelperClickListener() {
//
//                @Override
//                public void onBuyGreatMinerClicked(int amount) {
//
//                }
//            }, App.getCurrentUser().getGreatMinerIdFive(),5);
//        } else {
//            Toast toast = Toast.makeText(getActivity(),
//                    getString(R.string.full_upgrade), Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }
//
//    @OnClick(R.id.minerSix)
//    public void onMinersixClicked() {
//
//        if (App.getCurrentUser().getGreatMinerIdSix() != 5) {
//            GreatMinerBuyDialogHelper.show(getContext(), new GreatMinerBuyDialogHelper.GreatMinerBuyDialogHelperClickListener() {
//
//                @Override
//                public void onBuyGreatMinerClicked(int amount) {
//
//                }
//            }, App.getCurrentUser().getGreatMinerIdSix(),6);
//        } else {
//            Toast toast = Toast.makeText(getActivity(),
//                    getString(R.string.full_upgrade), Toast.LENGTH_SHORT);
//            toast.show();
//        }
//    }

    @OnClick(R.id.offers)
    public void onOffersClicked() {

        LocoMotif controller = LocoMotif.create(LocoMotif.Builder.create()
                .setCurrencyName(CurrencyHelper.getLocoCurrency(App.getCurrency()))
                .setWithOffers(true)
                .setColorAcent(Color.parseColor("#E8068E"))
                .setColorAcentPressed(Color.parseColor("#E8068E"))
                .setRewardMultiplier(CurrencyHelper.getLocoRewardMultiplier(App.getCurrency()))
                .setFormat(CurrencyHelper.getLocoFormat(App.getCurrency()))
                .setLocoRewardCallback(new LocoMotif.LocoRewardCallback() {
                    @Override
                    public float getMyBalance() {
                        return CurrencyHelper.getLocoCurrentBalance(App.getCurrency());
                    }

                    @Override
                    public void onTotalReward(float totalReward, float delta) {
                        User user = App.getCurrentUser();
                        user.increaseBalance(CurrencyHelper.getLocoTotalReward(App.getCurrency(), delta));
                        App.setCurrentUser(user);
                    }
                }));

        controller.open(getContext());
    }

    private void checkOffersTime() {
        if (isShownOffers()) {
            offers.setVisibility(View.VISIBLE);
//            bottomNavigation.getMenu().findItem(R.id.action_offers).setVisible(true);
        } else {
            offers.setVisibility(View.GONE);
//            bottomNavigation.getMenu().findItem(R.id.action_offers).setVisible(false);
        }
    }

    public static boolean isShownOffers() {

        long timeInstalled = AwsAppPreferenceManager.getInstance().getTimeInstalled();
        long timeNoOffers = AwsApp.getServerConfig().getTimeNoOffers();
        if (System.currentTimeMillis() - timeInstalled > timeNoOffers) {
            return true;
        }
        JunkProvider.f();

        return false;
    }




    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {

            if (!NetworkStateReceiver.connected) {
                ((AnimationDrawable) ivAnimated.getDrawable()).stop();
            }else{
                if(isAnimateStart){
                    ((AnimationDrawable) ivAnimated.getDrawable()).start();
                }
            }
            App.getUIHandler().postDelayed(timerRunnable, 1000);


//            boolean connection;
//            ConnectivityManager cm =
//                    (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//            if (activeNetwork == null){
//                connection = false;
//            }else {
//                connection = true;
//            }
//
//            if(!connection){
//                ivAnimated.setImageResource(R.drawable.load1);
//               // ((AnimationDrawable) ivAnimated.getDrawable()).stop();
//            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        App.getUIHandler().removeCallbacks(timerRunnable);
    }
}




