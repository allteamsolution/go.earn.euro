package earn.dollars.fast.helper;

import java.util.ArrayList;
import java.util.List;

public class ShopHelper {


    public static String getGreatMinerPower(int id) {
        switch (id) {
            case 1:
                return Constants.MINER_POWER_ONE_MAIN;
            case 2:
                return Constants.MINER_POWER_TWO_MAIN;
            case 3:
                return Constants.MINER_POWER_THREE_MAIN;
            case 4:
                return Constants.MINER_POWER_FOUR_MAIN;
            case 5:
                return Constants.MINER_POWER_FIVE_MAIN;
            default:
                return Constants.MINER_POWER_ZERO_MAIN;
        }
    }

    public static float getGreatMinerPowerToTextView(List<Integer> list) {
        float greatPower = 0;
        for (int power : list) {
            if(power==1){
                greatPower = greatPower + 0.05f;
            }else if( power ==2){
                greatPower = greatPower + 0.10f;
            }else if(power == 3) {
                greatPower = greatPower + 0.15f;
            }else if (power == 4){
                greatPower = greatPower + 0.20f;
            }else if (greatPower == 5){
                greatPower = greatPower + 0.25f;
            }else{
                greatPower = greatPower + 0.00f;
            }
        }

        return greatPower;
    }

    public static float getGreatMinerCost(int id) {
        switch (id) {
            case 1:
                return Constants.MINER_COST_ONE_FLOAT;
            case 2:
                return Constants.MINER_COST_TWO_FLOAT;
            case 3:
                return Constants.MINER_COST_THREE_FLOAT;
            case 4:
                return Constants.MINER_COST_FOUR_FLOAT;
            case 5:
                return Constants.MINER_COST_FIVE_FLOAT;
            default:
                return Constants.MINER_COST_ZERO_FLOAT;
        }
    }

    public static float getGreatMinerSpeed(int id) {
        switch (id) {
            case 1:
                return Constants.MINER_SPEED_ONE_FLOAT;
            case 2:
                return Constants.MINER_SPEED_TWO_FLOAT;
            case 3:
                return Constants.MINER_SPEED_THREE_FLOAT;
            case 4:
                return Constants.MINER_SPEED_FOUR_FLOAT;
            case 5:
                return Constants.MINER_SPEED_FIVE_FLOAT;
            default:
                return Constants.MINER_SPEED_ZERO_FLOAT;
        }
    }
}
