package earn.dollars.fast.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.helper.CurrencyHelper;

public class RateUsHelper extends BaseDialogHelper {

    private RateUsHelperCallback callback;

    @BindView(R.id.tvRateUs)
    protected TextView tvRateUs;

    public interface RateUsHelperCallback{
        void onGoneToGooglePlay();
    }

    public static void show(Context context, RateUsHelperCallback callback) {
        new RateUsHelper(context,callback);

    }

    private RateUsHelper(Context context, RateUsHelperCallback callback) {
        super(context);
        this.callback = callback;
    }

    public interface PrivacyDialogCallback{
        void onOpenPrivacy(String url);
    }

    @Override
    protected View onCreateView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.rate_on_gp, null);
        ButterKnife.bind(this, view);

        tvRateUs.setText(App.getContext().
                getResources().
                getString(R.string.premium_demo_descr,
                        String.format(
                                CurrencyHelper.getFormatBalance(App.getCurrency()),
                                CurrencyHelper.getApplyRefCodeReward(App.getCurrency())
                        )
                )
        );

        return view;
    }

    @OnClick(R.id.btnAgree)
    public void onbtnAgreeClick(){
        callback.onGoneToGooglePlay();
        dismiss();
    }
}
