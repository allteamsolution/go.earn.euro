package earn.dollars.fast.rest;


import earn.dollars.fast.rest.body.ApplyRefCodeBody;
import earn.dollars.fast.rest.body.GenerateRefCodeBody;
import earn.dollars.fast.rest.body.GetReferersBody;
import earn.dollars.fast.rest.response.ApplyRefCodeResponse;
import earn.dollars.fast.rest.response.GenerateRefCodeResponse;
import earn.dollars.fast.rest.response.GetReferersResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CraneApi {

    @POST("/api/crane/RegisterRefCode")
    Call<GenerateRefCodeResponse> registerRefCode(@Body GenerateRefCodeBody body);

    @POST("/api/crane/ApplyRefCode")
    Call<ApplyRefCodeResponse> applyRefCode(@Body ApplyRefCodeBody body);

    @POST("/api/crane/GetReferers")
    Call<GetReferersResponse> getReferrers(@Body GetReferersBody body);
}
