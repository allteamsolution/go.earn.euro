package earn.dollars.fast.control;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.Utility;

public class InAppNotificationControl {

    @BindView(R.id.llNotif)
    protected LinearLayout llNotif;

    @BindView(R.id.tvReward)
    protected TextView tvReward;

    private Random random = new Random();
    private static DecimalFormat df2 = new DecimalFormat("#.#####");

    public InAppNotificationControl(LinearLayout parent){
        ButterKnife.bind(this, parent);
    }

    public void show(){
        if (!Utility.hasInternet(App.getContext())){
            return;
        }
        JunkProvider.f();

        llNotif.setVisibility(View.VISIBLE);
        String userId = "0" + (10000 + random.nextInt(10000));
        double payout = CurrencyHelper.getMinPayout(App.getCurrency());

        tvReward.setText(tvReward.getContext().getString(R.string.riched_payout, userId, String.format(CurrencyHelper.getFormatPayout(App.getCurrency()), payout) + " " + App.getCurrency()));
        App.getUIHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                llNotif.setVisibility(View.GONE);
            }
        }, 4500);
    }

    @OnClick(R.id.llNotif)
    public void onllNotifClick(){
        llNotif.setVisibility(View.GONE);
    }

}
