package earn.dollars.fast.rest.body;


import com.google.gson.annotations.SerializedName;

public class GenerateRefCodeBody {

    @SerializedName("app")
    public String app;

    @SerializedName("token")
    public String token;

    public GenerateRefCodeBody(String app, String token) {
        this.app = app;
        this.token = token;
    }
}
