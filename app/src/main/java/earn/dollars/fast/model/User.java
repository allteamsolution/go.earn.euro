package earn.dollars.fast.model;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import earn.dollars.fast.App;
import earn.dollars.fast.adapter.ShopItem;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.RefCodeGenerator;


public class User {
        private static final int TIME_PREMIUM = 10 * 60 * 60 * 1000;


        final public static String BALANCE = "BALANCE";
        final public static String REF_BALANCE = "REF_BALANCE";
        final public static String COUNT_TAKE_REWARD = "COUNT_TAKE_REWARD";
        final public static String UI_REFCODE_APPLIED = "UI_REFCODE_APPLIED";
        final public static String MY_REFERRER = "MY_REFERRER";
        final public static String MY_REFERRERS_COUNT = "MY_REFERRERS_COUNT";
        final public static String UI_REF_CODE = "refCode";
        final public static String UI_VALUE_WALLET = "wallet";
        final public static String UI_USER_ID = "UI_USER_ID";
        final public static String TUTORIAL_FINISH = "TUTORIAL_FINISH";
        final public static String TIME_WATCH_REWARD = "TIME_WATCH_REWARD";
        final public static String      UI_NOTIFY_ENABLED = "notify";
        final public static String      UI_SEND_STAT = "sendStats";

        final public static String IS_DEMO_PREM = "IS_DEMO_PREM";
        final public static String DURATION_LAST_WITHDRAW = "DURATION_LAST_WITHDRAW";
        final public static String TIME_LAST_WITHDRAW = "TIME_LAST_WITHDRAW";
        final public static String BALANCE_LAST_WITHDRAW = "BALANCE_LAST_WITHDRAW";

        private static final String TIME_TO_WATCH_VIDEO_ONE = "TIME_TO_WATCH_VIDEO_ONE";
        private static final String TIME_TO_WATCH_VIDEO_TWO = "TIME_TO_WATCH_VIDEO_TWO";
        private static final String PRIZE_ONE = "PRIZE_ONE";
        private static final String PRIZE_TWO = "PRIZE_TWO";
        private static final String PRIZE_THREE = "PRIZE_THREE";
        private static final String PRIZE_FOUR = "PRIZE_FOUR";
        private static final String PRIZE_FIVE = "PRIZE_FIVE";
        private static final String PRIZE_SIX = "PRIZE_SIX";


        private static final String POWER = "POWER";
        private static final String GREAT_MINER_ID_ONE = "GREAT_MINER_ID_ONE";
        private static final String GREAT_MINER_ID_TWO = "GREAT_MINER_ID_TWO";
        private static final String GREAT_MINER_ID_THREE = "GREAT_MINER_ID_THREE";
        private static final String GREAT_MINER_ID_FOUR = "GREAT_MINER_ID_FOUR";
        private static final String GREAT_MINER_ID_FIVE = "GREAT_MINER_ID_FIVE";
        private static final String GREAT_MINER_ID_SIX = "GREAT_MINER_ID_SIX";

    final public static String TIME_DEMO_PREM = "TIME_DEMO_PREM";


    public final static int PLAN_DEF = 1;
    public final static int PLAN_PREMIUM = 2;
    public final static int PLAN_PREMIUM_DEMO = 4;
    public final static int PLAN_PLATINUM = 3;

    final public static String TIME_LAST_RESTART = "TIME_LAST_RESTART";



        final public static String      UI_SPIN_COUNT = "spin_count";
        final public static String      UI_LAST_SPIN_TIME = "UI_LAST_SPIN_TIME";
        final public static String      UI_ROULETTE_WIN_COUNT = "UI_ROULETTE_WIN_COUNT";
        final public static String      UI_ROULETTE_ROOLS_NO_WIN_COUNT = "UI_ROULETTE_ROOLS_NO_WIN_COUNT";
        final public static String      LAST_SPIN_CHANGE = "LAST_SPIN_CHANGE";



        private static float balance, refBalance, balanceLastWithdraw;
        private int countTakeReward, durationLastWithdraw, myReferrersCount;
        private long lastWatchingAdd, timeLastWithdraw;
        private String refCode, myReferrer, walletAddress, userId;
        private boolean refCodeApplied, tutorialFinish, demoPremiumUsed;

        public long timeToWatchVideoOne = 0;
        public long timeToWatchVideoTwo = 0;
        public long timeLastRestart;
        public long timeDemoPremiumStart;

        public long prizeOne = 0;
        public long prizeTwo = 0;
        public long prizeThree = 0;
        public long prizeFour = 0;
        public long prizeFive = 0;
        public long prizeSix = 0;
        public int plan;

        public float power;

        public int greatMinerIdOne;
        public int greatMinerIdTwo;
        public int greatMinerIdThree;
        public int greatMinerIdFour;
        public int greatMinerIdFive;
        public int greatMinerIdSix;




    public boolean notifyEnabled;

        public boolean sendAnonStatistics;
        public int rouletteRollsNoWinsCount;
        public int rouletteWinCount;
        private int spinCount;
        public int lastSpinChange;
        public long lastFreeSpinTime;


        public void increaseBalance(double reward) {
        this.balance += reward;
    }
        public void increaseRefBalance(double reward) { this.refBalance += reward; }

    public boolean isDemoPremiumActive(){
        if (timeDemoPremiumStart + TIME_PREMIUM > System.currentTimeMillis()){
            return true;
        }

        return false;
    }

        public void save() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putFloat(BALANCE, balance);
        storage.putFloat(REF_BALANCE, refBalance);
        storage.putInt(COUNT_TAKE_REWARD, countTakeReward);
        storage.putString(UI_USER_ID, userId);
        storage.putLong(TIME_DEMO_PREM, timeDemoPremiumStart);

        storage.putLong(TIME_WATCH_REWARD, lastWatchingAdd);

        storage.putString(UI_REF_CODE, refCode);
        storage.putString(MY_REFERRER, myReferrer);
        storage.putBoolean(UI_REFCODE_APPLIED, refCodeApplied);
        storage.putBoolean(IS_DEMO_PREM, demoPremiumUsed);

        storage.putBoolean(TUTORIAL_FINISH, tutorialFinish);
        storage.putString(UI_VALUE_WALLET, walletAddress);
        storage.putLong(TIME_TO_WATCH_VIDEO_ONE, timeToWatchVideoOne);
        storage.putLong(TIME_TO_WATCH_VIDEO_TWO, timeToWatchVideoTwo);
        storage.putLong(TIME_LAST_RESTART, timeLastRestart);

        storage.putLong(PRIZE_ONE, prizeOne);
        storage.putLong(PRIZE_TWO, prizeTwo);
        storage.putLong(PRIZE_THREE, prizeThree);
        storage.putLong(PRIZE_FOUR, prizeFour);
        storage.putLong(PRIZE_FIVE, prizeFive);
        storage.putLong(PRIZE_SIX, prizeSix);

        storage.putFloat(POWER, power);

        storage.putInt(GREAT_MINER_ID_ONE, greatMinerIdOne);
        storage.putInt(GREAT_MINER_ID_TWO, greatMinerIdTwo);
        storage.putInt(GREAT_MINER_ID_THREE, greatMinerIdThree);
        storage.putInt(GREAT_MINER_ID_FOUR, greatMinerIdFour);
        storage.putInt(GREAT_MINER_ID_FIVE, greatMinerIdFive);
        storage.putInt(GREAT_MINER_ID_SIX, greatMinerIdSix);



        storage.putBoolean(UI_NOTIFY_ENABLED, notifyEnabled);
        storage.putBoolean(UI_SEND_STAT, sendAnonStatistics);

        storage.putInt(DURATION_LAST_WITHDRAW, durationLastWithdraw);
        storage.putInt(MY_REFERRERS_COUNT, myReferrersCount);
        storage.putLong(TIME_LAST_WITHDRAW, timeLastWithdraw);
        storage.putFloat(BALANCE_LAST_WITHDRAW, balanceLastWithdraw);

        storage.putInt(LAST_SPIN_CHANGE, lastSpinChange);
        storage.putInt(UI_ROULETTE_ROOLS_NO_WIN_COUNT, rouletteRollsNoWinsCount);
        storage.putInt(UI_ROULETTE_WIN_COUNT, rouletteWinCount);
        storage.putInt(UI_SPIN_COUNT, spinCount);
        storage.putLong(UI_LAST_SPIN_TIME, lastFreeSpinTime);


        storage.apply();
    }

        public void load() {
        final SharedPreferences storage = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        balance = storage.getFloat(BALANCE, 0L);
        refBalance = storage.getFloat(REF_BALANCE, 0L);
        countTakeReward = storage.getInt(COUNT_TAKE_REWARD, 0);
        lastWatchingAdd = storage.getLong(TIME_WATCH_REWARD, 0L);

        refCode = storage.getString(UI_REF_CODE, "");
        myReferrer = storage.getString(MY_REFERRER, "");
        refCodeApplied = storage.getBoolean(UI_REFCODE_APPLIED, false);
        tutorialFinish = storage.getBoolean(TUTORIAL_FINISH, false);
        demoPremiumUsed =  storage.getBoolean(IS_DEMO_PREM, false);
        notifyEnabled = storage.getBoolean(UI_NOTIFY_ENABLED, true);
        sendAnonStatistics = storage.getBoolean(UI_SEND_STAT, true);

        walletAddress = storage.getString(UI_VALUE_WALLET, "");
        userId = storage.getString(UI_USER_ID, RefCodeGenerator.generateUserId());

        durationLastWithdraw = storage.getInt(DURATION_LAST_WITHDRAW, 0);
        myReferrersCount = storage.getInt(MY_REFERRERS_COUNT, 0);
        timeLastWithdraw = storage.getLong(TIME_LAST_WITHDRAW, 0);
        balanceLastWithdraw = storage.getFloat(BALANCE_LAST_WITHDRAW, 0f);
        timeToWatchVideoOne = storage.getLong(TIME_TO_WATCH_VIDEO_ONE, 0L);
        timeToWatchVideoTwo = storage.getLong(TIME_TO_WATCH_VIDEO_TWO, 0L);

            timeLastRestart = storage.getLong(TIME_LAST_RESTART, 0L);
            timeDemoPremiumStart = storage.getLong(TIME_DEMO_PREM, 0L);


            prizeOne = storage.getLong(PRIZE_ONE, 0L);
            prizeTwo = storage.getLong(PRIZE_TWO, 0L);
            prizeThree = storage.getLong(PRIZE_THREE, 0L);
            prizeFour = storage.getLong(PRIZE_FOUR, 0L);
            prizeFive = storage.getLong(PRIZE_FIVE, 0L);
            prizeSix = storage.getLong(PRIZE_SIX, 0L);

            power = storage.getFloat(POWER, 0f);

            greatMinerIdOne = storage.getInt(GREAT_MINER_ID_ONE, 0);
            greatMinerIdTwo = storage.getInt(GREAT_MINER_ID_TWO, 0);
            greatMinerIdThree = storage.getInt(GREAT_MINER_ID_THREE, 0);
            greatMinerIdFour = storage.getInt(GREAT_MINER_ID_FOUR, 0);
            greatMinerIdFive = storage.getInt(GREAT_MINER_ID_FIVE, 0);
            greatMinerIdSix = storage.getInt(GREAT_MINER_ID_SIX, 0);

        lastSpinChange = storage.getInt(LAST_SPIN_CHANGE, 0);
        rouletteRollsNoWinsCount = storage.getInt(UI_ROULETTE_ROOLS_NO_WIN_COUNT, 0);
        rouletteWinCount = storage.getInt(UI_ROULETTE_WIN_COUNT, 0);
        spinCount = storage.getInt(UI_SPIN_COUNT, 1);
        lastFreeSpinTime = storage.getLong(UI_LAST_SPIN_TIME, -1L);
    }

        public void clear() {
        balance = 0;
        refBalance = 0;
        myReferrersCount = 0;
        countTakeReward = 0;
        lastWatchingAdd = 0;
        refCode = RefCodeGenerator.generate();
        myReferrer = "";
        refCodeApplied = false;
        tutorialFinish = false;
        demoPremiumUsed = false;
        sendAnonStatistics = true;
        notifyEnabled = true;
        walletAddress = "";
        userId = "";
        durationLastWithdraw = 0;
        timeLastWithdraw = 0;
        balanceLastWithdraw = 0;

        lastSpinChange = 0;
        rouletteRollsNoWinsCount = 0;
        rouletteWinCount = 0;
        spinCount = 1;
        lastFreeSpinTime = -1L;

        save();
    }

        public String getRefCode() { return refCode; }
        public double getBalance() { return balance; }
        public int getCountTakeReward() { return countTakeReward; }

    public void setBalance(float balance) {
        this.balance = balance;
    }

        public void setCountTakeReward(int countTakeReward) {
        this.countTakeReward = countTakeReward;
    }

        public long getLastWatchingAdd() {
        return lastWatchingAdd;
    }

        public void setLastWatchingAdd(long lastWatchingAdd) {
        this.lastWatchingAdd = lastWatchingAdd;
    }

        public int getDurationLastWithdraw() {
        return durationLastWithdraw;
    }

        public void setDurationLastWithdraw(int durationLastWithdraw) {
        this.durationLastWithdraw = durationLastWithdraw;
    }

        public long getTimeLastWithdraw() {
        return timeLastWithdraw;
    }

        public void setTimeLastWithdraw(long timeLastWithdraw) {
        this.timeLastWithdraw = timeLastWithdraw;
    }

        public double getBalanceLastWithdraw() {
        return balanceLastWithdraw;
    }

        public void setBalanceLastWithdraw(float balanceLastWithdraw) {
        this.balanceLastWithdraw = balanceLastWithdraw;
    }

        public String getWalletAddress() {
        return walletAddress;
    }

        public void setWalletAddress(String walletAddress) {
        this.walletAddress = walletAddress;
    }

        public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

        public boolean isRefCodeApplied() {
        return refCodeApplied;
    }

        public void setRefCodeApplied(boolean refCodeApplied) {
        this.refCodeApplied = refCodeApplied;
    }

        public float getRefBalance() {
        return refBalance;
    }

        public void setRefBalance(float refBalance) {
        this.refBalance = refBalance;
    }

        public String getMyReferrer() {
        return myReferrer;
    }

        public void setMyReferrer(String myReferrer) {
        this.myReferrer = myReferrer;
    }

        public int getMyReferrersCount() {
        return myReferrersCount;
    }

        public void setMyReferrersCount(int myReferrersCount) {
        this.myReferrersCount = myReferrersCount;
    }

        public boolean isTutorialFinish() {
        return tutorialFinish;
    }

        public void setTutorialFinish(boolean tutorialFinish) {
        this.tutorialFinish = tutorialFinish;
    }

        public boolean isDemoPremiumUsed() {
        return demoPremiumUsed;
    }

        public void setDemoPremiumUsed(boolean demoPremiumUsed) {
        this.demoPremiumUsed = demoPremiumUsed;
    }

        public long getTimeToWatchVideoOne() {
        return timeToWatchVideoOne;
    }

        public void setTimeToWatchVideoOne(long timeToWatchVideoOne) {
        this.timeToWatchVideoOne = timeToWatchVideoOne;
    }



        public long getTimeToWatchVideoTwo() {
        return timeToWatchVideoTwo;
    }

        public void setTimeToWatchVideoTwo(long timeToWatchVideoTwo) {
        this.timeToWatchVideoTwo = timeToWatchVideoTwo;
    }

    public int getSpinCount() {
        return spinCount;
    }

    public void changeSpinCount(int change) {
        this.spinCount += change;
        this.lastSpinChange = change;
    }

    public void setSpinCount(int newSpinCount){
        this.lastSpinChange = newSpinCount - this.spinCount;
        this.spinCount = newSpinCount;
    }

    public long getPrizeOne() {
        return prizeOne;
    }

    public void setPrizeOne(long prizeOne) {
        this.prizeOne = prizeOne;
    }

    public long getPrizeTwo() {
        return prizeTwo;
    }

    public void setPrizeTwo(long prizeTwo) {
        this.prizeTwo = prizeTwo;
    }

    public long getPrizeThree() {
        return prizeThree;
    }

    public void setPrizeThree(long prizeThree) {
        this.prizeThree = prizeThree;
    }

    public long getPrizeFour() {
        return prizeFour;
    }

    public void setPrizeFour(long prizeFour) {
        this.prizeFour = prizeFour;
    }

    public long getPrizeFive() {
        return prizeFive;
    }

    public void setPrizeFive(long prizeFive) {
        this.prizeFive = prizeFive;
    }

    public long getPrizeSix() {
        return prizeSix;
    }

    public float getPower(){
            return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

    public  int getGreatMinerIdOne() {
        return greatMinerIdOne;
    }

    public  int getGreatMinerIdTwo() {
        return greatMinerIdTwo;
    }

    public  int getGreatMinerIdThree() {
        return greatMinerIdThree;
    }

    public  int getGreatMinerIdFour() {
        return greatMinerIdFour;
    }

    public  int getGreatMinerIdFive() {
        return greatMinerIdFive;
    }

    public  int getGreatMinerIdSix() {
        return greatMinerIdSix;
    }

    public void setGreatMinerIdOne(int greatMinerIdOne) {
        this.greatMinerIdOne = greatMinerIdOne;
    }

    public void setGreatMinerIdTwo(int greatMinerIdTwo) {
        this.greatMinerIdTwo = greatMinerIdTwo;
    }

    public void setGreatMinerIdThree(int greatMinerIdThree) {
        this.greatMinerIdThree = greatMinerIdThree;
    }

    public void setGreatMinerIdFour(int greatMinerIdFour) {
        this.greatMinerIdFour = greatMinerIdFour;
    }

    public void setGreatMinerIdFive(int greatMinerIdFive) {
        this.greatMinerIdFive = greatMinerIdFive;
    }

    public void setGreatMinerIdSix(int greatMinerIdSix) {
        this.greatMinerIdSix = greatMinerIdSix;
    }

    public void setPrizeSix(long prizeSix) {
        this.prizeSix = prizeSix;
    }

    public float getPlanReward() {
        switch (plan) {
            case PLAN_DEF:
                return Constants.DEF_PLAN_REW;
            case PLAN_PREMIUM:
                return Constants.PREMIUM_PLAN_REW;
            case PLAN_PREMIUM_DEMO:
                if (isDemoPremiumActive()) {
                    return Constants.PREMIUM_PLAN_REW;
                } else {
                    plan = User.PLAN_DEF;
                    save();
                    return Constants.DEF_PLAN_REW;
                }
            case PLAN_PLATINUM:
                return Constants.PLATINUM_PLAN_REW;
        }

        return 0f;
    }
}
