package earn.dollars.fast.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import butterknife.ButterKnife;
import butterknife.OnClick;
import earn.dollars.fast.R;
import fx.helper.LocalHelper;
import fx.helper.news.NewsFeedCallback;
import fx.helper.news.NewsFeedFragment;

public class NewsFeedActivity extends AppCompatActivity implements NewsFeedCallback {

    private FrameLayout flNews;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_item_activity);
        flNews = findViewById(R.id.flNews);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getString(R.string.news));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            NewsFeedFragment curFragment = NewsFeedFragment.newInstance();

            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.flNews, curFragment)
                    .commitAllowingStateLoss();
        }
    }

    @Override
    public void onNewsItemClicked(String url) {
        //TODO Open in your browser clergy.pencil.activity (NewsItemActivity)
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (android.content.ActivityNotFoundException ex) {}
    }

    @Override
    public String getFeedUrl() {
        String urlString;
        if (LocalHelper.isRuRegion()) {
            urlString = "https://lenta-novostei.com/feed";
        } else {
            urlString = "https://crypto-news.space/en/" + "feed/";
        }

        return urlString;
    }

    @Override
    public Drawable getNewsItemPlaceholder() {
        return getResources().getDrawable(R.drawable.gs_back_card_g);
    }

    @Override
    public Point getNewsPlaceholderImageSizeInDp() {
        return new Point(140, 100);
    }

    @Override
    public int getNewsItemLayoutId() {
        return R.layout.news_item;
    }

    @Override
    public int getCustomAdItemLayoutId() {
        return R.layout.custom_ad_item;
    }

    @Override
    public boolean showAdsInAdapter() {
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
