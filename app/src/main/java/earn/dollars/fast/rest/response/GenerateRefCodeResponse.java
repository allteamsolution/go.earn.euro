package earn.dollars.fast.rest.response;

import com.google.gson.annotations.SerializedName;

public class GenerateRefCodeResponse {

	@SerializedName("result")
	private String result;

	@SerializedName("error")
	private String error;

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("success")
	private boolean success;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
 	public String toString(){
		return
			"GenerateRefCodeResponse{" +
			"result = '" + result + '\'' +
			",success = '" + success + '\'' +
			",error = '" + error + '\'' +
			",error_msg = '" + errorMsg + '\'' +
			"}";
		}
}