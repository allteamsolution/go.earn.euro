package earn.dollars.fast;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

public class CustomDrawableProgressBar extends RoundCornerProgressBar {

    private Drawable rcProgressDrawable;
    final static int ANIMATION_DURATION = 1000;

    public CustomDrawableProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomDrawableProgressBar);

        Drawable progressDrawable = attributes.getDrawable(R.styleable.CustomDrawableProgressBar_rcProgressDrawable);
        if(progressDrawable != null){
            rcProgressDrawable = progressDrawable;
        }

        attributes.recycle();
    }

    public CustomDrawableProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomDrawableProgressBar, defStyleAttr, 0);

        Drawable progressDrawable = attributes.getDrawable(R.styleable.CustomDrawableProgressBar_rcProgressDrawable);
        if(progressDrawable != null){
            rcProgressDrawable = progressDrawable;
        }

        attributes.recycle();
    }

    public void animateView(double fromProgress, double toProgress) {
        if (fromProgress != toProgress) {
            ValueAnimator mProgressAnimator = new ValueAnimator();

            mProgressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                    @Override
                                                    public void onAnimationUpdate(ValueAnimator animation) {
                                                        setProgress((Float) animation.getAnimatedValue());
                                                    }
                                                }
            );

            mProgressAnimator.setDuration(ANIMATION_DURATION);
            mProgressAnimator.setFloatValues((float)fromProgress, (float)toProgress);
            mProgressAnimator.start();
        }
    }


    @SuppressWarnings("deprecation")
    @Override
    protected void drawProgress(LinearLayout layoutProgress, float max, float progress, float totalWidth,
                                int radius, int padding, int colorProgress, boolean isReverse) {

        super.drawProgress(layoutProgress, max, progress, totalWidth, radius, padding, colorProgress, isReverse);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if(rcProgressDrawable != null){
                layoutProgress.setBackground(rcProgressDrawable);
            }
        }
    }
}
