package earn.dollars.fast.rest.body;


import com.google.gson.annotations.SerializedName;

public class ApplyRefCodeBody {

    @SerializedName("my_ref_code")
    public String myRefCode;

    @SerializedName("applied_ref_code")
    public String appliedRefCode;

    public ApplyRefCodeBody(String myRefCode, String appliedRefCode) {
        this.myRefCode = myRefCode;
        this.appliedRefCode = appliedRefCode;
    }
}
