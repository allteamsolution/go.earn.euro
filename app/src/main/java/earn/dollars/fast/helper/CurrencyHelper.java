package earn.dollars.fast.helper;


import earn.dollars.fast.App;

public class CurrencyHelper {
    public static double getMinPayout(String currency) {
        switch (currency) {
            case Constants.BTC:
                return 0.005;
            case Constants.EUR:
                return 20;
            case Constants.USD:
                return 20;
            case Constants.RUB:
                return 1500;
            default:
                return 0.005;
        }
    }

    public static String  getFormatBalanceForPrize(String currency) {
        switch (currency) {
            case Constants.BTC:
                return "%.8f";
            case Constants.EUR:
                return "%.6f";
            case Constants.USD:
                return "%.6f";
            case Constants.RUB:
                return "%.2f";
            default:
                return "%.8f";
        }
    }
    public static double getPricePayoutFast(String currency) {
        return getMinPayout(currency) * 0.05;
    }

    public static double getPricePayoutNormal(String currency) {
        return getMinPayout(currency) * 0.01;
    }

    public static String getFormatBalance(String currency) {
        switch (currency) {
            case Constants.BTC:
                return "%.8f";
            case Constants.EUR:
                return "%.3f";
            case Constants.USD:
                return "%.3f";
            case Constants.RUB:
                return "%.2f";
            default:
                return "%.8f";
        }
    }

    public static String getFormatPayout(String currency) {
        switch (currency) {
            case Constants.BTC:
                return "%.3f";
            case Constants.EUR:
                return "%.2f";
            case Constants.USD:
                return "%.2f";
            case Constants.RUB:
                return "%.2f";
            default:
                return "%.3f";
        }
    }

    public static Double getLotoReward(String currency, int number) {
        double result = 0;
        if (number >= Constants.AMAZING_NUMBER) {
            result = getMinPayout(currency) * 0.1;
        } else if (number > Constants.MEDIUM_HIGH_NUMBER) {
            result = getMinPayout(currency) * 0.0005;
        } else if (number > Constants.MEDIUM_LOW_NUMBER) {
            result = getMinPayout(currency) * 0.0003;
        } else if (number > Constants.LOW_NUMBER) {
            result = getMinPayout(currency) * 0.0002;
        } else {
            result =  getMinPayout(currency) * 0.0001;
        }
        return result;
    }

    public static String getLocoCurrency(String currency) {
        switch (currency) {
            case Constants.BTC:
                return "satoshi";
            case Constants.EUR:
                return "cents";
            case Constants.USD:
                return "cents";
            case Constants.RUB:
                return "cop";
            default:
                return "satoshi";
        }
    }

    public static float getLocoRewardMultiplier(String currency) {
        switch (currency) {
            case Constants.BTC:
                return 10;
            case Constants.EUR:
                return 0.4f;
            case Constants.USD:
                return 0.4f;
            case Constants.RUB:
                return 10;
            default:
                return 10;
        }
    }

    public static float getLocoCurrentBalance(String currency) {
        double balance = App.getCurrentUser().getBalance();
        switch (currency) {
            case Constants.BTC:
                return (float)(balance * Constants.btcToSatoshi);
            case Constants.EUR:
                balance = balance * 100.0000 / 100.0000;
                return (float)(balance * 100);
            case Constants.USD:
                balance = balance * 100.0000 / 100.0000;
                return (float)(balance * 100);
            case Constants.RUB:
                balance = balance * 100.0000 / 100.0000;
                return (float)(balance * 100);
            default:
                return (float)(balance * Constants.btcToSatoshi);
        }
    }

    public static double getLocoTotalReward(String currency, float delta) {
        switch (currency) {
            case Constants.BTC:
                return delta * Constants.satoshiToBtc;
            case Constants.EUR:
                return (delta * 1.00f)/ 100;
            case Constants.USD:
                return (delta * 1.00f)/ 100;
            case Constants.RUB:
                return (delta * 1.00f)/ 100;
            default:
                return delta * Constants.satoshiToBtc;
        }
    }

    public static int getLocoFormat(String currency) {
        switch (currency) {
            case Constants.BTC:
                return 0;
            case Constants.EUR:
                return 2;
            case Constants.USD:
                return 2;
            case Constants.RUB:
                return 0;
            default:
                return 0;
        }
    }

    public static double getApplyRefCodeReward(String currency) {
        return getMinPayout(currency) * 0.005;
    }

    public static double getRefCodeRewardPerReferer(String currency) {
        return getMinPayout(currency) * 0.001;
    }
}
