package earn.dollars.fast.fragment;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.model.User;
import earn.dollars.fast.rest.body.ApplyRefCodeBody;
import earn.dollars.fast.rest.response.ApplyRefCodeResponse;
import great.utils.UIHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PayoutFragment extends Fragment  {


    @BindView(R.id.etWallet)
    protected EditText etWallet;

    @BindView(R.id.tvPayoutBalance)
    protected TextView tvBalance;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payout_fragment, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }


    private void initViews() {
        if (!App.getCurrentUser().getWalletAddress().isEmpty())
            etWallet.setText(App.getCurrentUser().getWalletAddress());

        User user = App.getCurrentUser();
        double balance = user.getBalance();
        tvBalance.setText("" + String.format("%.7f", balance) + " " + App.getCurrency());


    }

    @OnClick(R.id.btnGetBtc)
    protected void onPayoutClick() {
        double balance = App.getCurrentUser().getBalance();
        double minPayout = CurrencyHelper.getMinPayout(App.getCurrency());

        if (etWallet.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.enter_wallet), Toast.LENGTH_LONG).show();
            return;
        }
        if (balance < minPayout) {
            Toast.makeText(getActivity(), getResources().getString(R.string.min_payout_is, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getMinPayout(App.getCurrency())), App.getCurrency()), Toast.LENGTH_LONG).show();
            return;
        }

        if(balance >minPayout){
            App.getCurrentUser().increaseBalance(-balance);
            App.getCurrentUser().save();
            return;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (!etWallet.getText().toString().isEmpty())
            App.getCurrentUser().setWalletAddress(etWallet.getText().toString());
        App.update();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!etWallet.getText().toString().isEmpty())
            App.getCurrentUser().setWalletAddress(etWallet.getText().toString());
        App.update();
    }

}

