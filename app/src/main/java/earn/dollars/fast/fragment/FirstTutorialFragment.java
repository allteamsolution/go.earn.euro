package earn.dollars.fast.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.activity.TutorialActivity;
import earn.dollars.fast.helper.Utility;

public class FirstTutorialFragment extends BaseTutorialFragment {

    @BindView(R.id.ivArrowBack)
    protected ImageView arrowOne;

    @BindView(R.id.ivArrowForvard)
    protected ImageView arrowTwo;

    @BindView(R.id.pager2)
    protected ImageView pager2;

    public static FirstTutorialFragment newInstance(TutorialActivity activity) {
        FirstTutorialFragment fragment = new FirstTutorialFragment();
        fragment.activity = activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first_tutorial, container, false);
        unbinder = ButterKnife.bind(this, view);

        Utility.setDrawableColor(arrowOne, App.getContext().getResources().getColor(R.color.orange));
        Utility.setDrawableColor(arrowTwo, App.getContext().getResources().getColor(R.color.orange));
        Utility.setDrawableColor(pager2, App.getContext().getResources().getColor(R.color.orange));

        return view;
    }
}
