package earn.dollars.fast.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.adapter.TutorialViewPagerAdapter;
import earn.dollars.fast.anim.CubeTransformer;
import earn.dollars.fast.rest.body.GenerateRefCodeBody;
import earn.dollars.fast.rest.response.GenerateRefCodeResponse;
import fx.helper.loco.DeviceIdHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TutorialActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    protected ViewPager viewPager;

    private TutorialViewPagerAdapter tutorialViewPagerAdapter;
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        if (App.getCurrentUser().getRefCode().isEmpty()) {
            registerRefCode();
        }
        initViewPager();
    }

    private void initViewPager() {
        tutorialViewPagerAdapter= new TutorialViewPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setPageTransformer(true, new CubeTransformer());
        viewPager.setAdapter(tutorialViewPagerAdapter);
        viewPager.setOffscreenPageLimit(TutorialViewPagerAdapter.NUM_PAGES);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void registerRefCode() {
        String deviceId = DeviceIdHelper.id();
        String packageName = App.getContext().getPackageName();
        Call<GenerateRefCodeResponse> call = App.getCraneApi().registerRefCode(new GenerateRefCodeBody(packageName, deviceId));

        call.enqueue(new Callback<GenerateRefCodeResponse>() {
            @Override
            public void onResponse(Call<GenerateRefCodeResponse> call, Response<GenerateRefCodeResponse> response) {
                GenerateRefCodeResponse resp = response.body();
                if (!resp.getResult().isEmpty()) {
                    App.getCurrentUser().setRefCode(resp.getResult());
                    App.getCurrentUser().save();
                }

            }

            @Override
            public void onFailure(Call<GenerateRefCodeResponse> call, Throwable t) {

            }
        });


    }

    public void goNext(int index) {
        if (currentPage == TutorialViewPagerAdapter.NUM_PAGES - 1 && index > 0) {
            App.getCurrentUser().setTutorialFinish(true);
            App.getCurrentUser().save();
            startActivity(new Intent(this, MainActivity.class));
            finish();
            overridePendingTransition(R.anim.activity_alpha_fade_in, R.anim.activity_alpha_fade_out);
            return;
        }

        if (index > 0) {
            viewPager.setCurrentItem(currentPage + 1);
        } else {
            viewPager.setCurrentItem(currentPage - 1);
        }
    }
}
