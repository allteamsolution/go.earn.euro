package earn.dollars.fast;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.StringRes;

import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.rest.CraneApi;
import earn.dollars.fast.model.User;
import fx.GreatSolution;
import fx.LibConfigs;
import fx.helper.StringEncoder8;
import great.utils.UIHelper;
import rateusdialoghelper.RateDialogHelper;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class App extends Application {

    private static Context context;
    private static User currentUser;
    private static float maining_value;
    private static Handler handler;
    private static String currency;
    private static CraneApi craneApi;
    public  static boolean isMining = false;
    public static boolean startMining = false;
    public static float MINING_BONUS ;

    @Override
    public void onCreate() {
        super.onCreate();
        currency = Constants.EUR;
        context = getApplicationContext();
        handler = new Handler();
        RateDialogHelper.onNewSession(context);
        //UIHelper.init(context);
        initGreatLib();

//        if (getCurrentUser().getDurationLastWithdraw() != 0 && getCurrentUser().getDurationLastWithdraw() * 1000 * 3600 > getCurrentUser().getTimeLastWithdraw())
//            getCurrentUser().clear();
    }

    private void initGreatLib() {

        LibConfigs awsConfigs = new LibConfigs.Builder()
                .setDomain(StringEncoder8.decode("huvsw?)(kfdm>il!s~\u007F"))
                .setDebugTag("ApiTag")
                .setShowLogs(BuildConfig.DEBUG)
                .build(getApplicationContext());

        GreatSolution.Builder builder = new GreatSolution.Builder(getPackageName())
                .setAppName(getResources().getString(R.string.app_name))
                .setLibConfigs(awsConfigs)
                .setLogsEnabled(false)//used to see debug logs. Must be false for release builds
                .setAdsEvery5Sec(false)//used to check ads. Must be false for release builds
                .build(getApplicationContext());

        GreatSolution.init(builder);
    }

    public static CraneApi getCraneApi() {
        if (craneApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://cranedb.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            craneApi = retrofit.create(CraneApi.class);
        }
        return craneApi;
    }

    public static User getCurrentUser() {
        if (currentUser == null){
            currentUser = new User();
            currentUser.load();
        }

        return currentUser;
    }

    public static void setCurrentUser(User _currentUser) {
        currentUser = _currentUser;
        currentUser.save();
    }

    public static void update() {
        currentUser = App.getCurrentUser();
        currentUser.save();
    }

    public static Context getContext(){
        return context;
    }

    public static String getStringById(@StringRes int id){
        return context.getResources().getString(id);
    }


    public static Handler getUIHandler(){
        return handler;
    }

    public static String getCurrency() {
        return currency;
    }
}
