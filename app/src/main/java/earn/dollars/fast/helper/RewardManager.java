package earn.dollars.fast.helper;

import android.util.Log;

import java.util.Random;

import earn.dollars.fast.App;


public class RewardManager {

    private static int lowPay = 2000;
    private static int highPay = 4000;

    private static Random random = new Random();

    public static double reward(){
        double reward = countReward(App.getCurrentUser().getBalance());
        log(reward);
        return reward;
    }

    public static double getConstReward(){
        double reward = countReward(App.getCurrentUser().getBalance(), true);
        log(reward);
        return reward;
    }

    public static double countReward(double balance){
        return countReward(balance, false);
    }

    /**
     * for test purposes
     */
    public static double countReward(double balance, boolean noRandom){
        balance = balance * Constants.dollarToBtcCoef;
        if (balance == 0){
            return Constants.satoshiToBtc * 4000 * Constants.btcToDollarCoef;
        }

        double difference = Constants.minPayoutBtc - balance;

        if (difference < Constants.satoshiToBtc * 100){
            return 0;
        }

        if (difference < Constants.satoshiToBtc * 10000){
            int r = random.nextInt(2) + 1;
            return Constants.satoshiToBtc * r * Constants.btcToDollarCoef;
        }

        if (difference < Constants.satoshiToBtc * 30000){
            int r = random.nextInt(5) + 1;
            return Constants.satoshiToBtc * r * Constants.btcToDollarCoef;
        }


        if (balance < Constants.minPayoutBtc / 2){
            return (double)(lowPay + (noRandom ? highPay/2 : random.nextInt(highPay)) ) * Constants.satoshiToBtc * Constants.btcToDollarCoef;
        }

        int mnoz = 2;
        double sum = Constants.minPayoutBtc / 2 + Constants.minPayoutBtc / 4 ;
        while (balance > sum){
            mnoz *= 2;
            sum += Constants.minPayoutBtc / (mnoz * 2);
        }

        return (double)( lowPay / mnoz + (noRandom ? highPay / (mnoz * 2) : random.nextInt(highPay / mnoz)) ) * Constants.satoshiToBtc * Constants.btcToDollarCoef;
    }

    private static void log(double rew){
        double satoshi = rew * 100 * 1000 * 1000;
        Log.d("Reward", "Reward : " + satoshi + " Satoshi");
    }


    public static double getRandomReward(){
        double a = 0.001; // Начальное значение диапазона - "от"
        double b = 0.0006; // Конечное значение диапазона - "до"
        double reward = a + Math.random() * b;

        return reward;
    }

    public static double getRandomCoinsReward(){
        double a = 0.001; // Начальное значение диапазона - "от"
        double b = 0.0008; // Конечное значение диапазона - "до"
        double reward = a + Math.random() * b;

        return reward;
    }

}
