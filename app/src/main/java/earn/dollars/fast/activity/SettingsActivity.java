package earn.dollars.fast.activity;

import android.app.ActionBar;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.rest.body.ApplyRefCodeBody;
import earn.dollars.fast.rest.response.ApplyRefCodeResponse;
import fx.AwsApp;
import fx.helper.GDPRWidget;
import great.utils.UIHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity {


    @BindView(R.id.flGdpr)
    protected FrameLayout flGdpr;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
       // getSupportActionBar().setTitle(getString(R.string.settings_title));
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initGDPRWidget();
        recolorSwitch();
        changeIconGDPRWidget();
        changeTextColorGDPRWidget();
        //init();
    }


    @OnClick(R.id.ll1_site)
    public void onSiteClick() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = AwsApp.getServerConfig().getSiteUrl();
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (Exception ex) {}
    }

    @OnClick(R.id.ll2_privacy)
    public void onPrivacyPolicyClick() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = AwsApp.getServerConfig().getPrivacyPolicyUrl();
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (Exception ex) {}

    }

    @OnClick(R.id.ll3_support)
    public void onSupportFaqClick() {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + Constants.SUPPORT_EMAIL));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Constants.SUPPORT_EMAIL});
            startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_via)));
        }catch (Exception ex) {}
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
    private void initGDPRWidget() {
        GDPRWidget.create(flGdpr)
                .withDescrTextColor(getResources().getColor(R.color.backgroundMainTeam))
                .withIconColor(getResources().getColor(R.color.green))
                .withTitleTextColor(getResources().getColor(R.color.backgroundMainTeam))
                .withTitleTextSize(16)
                .withDescrTextSize(12)
                .build();
    }


    private void recolorSwitch() {
        int childCount = flGdpr.getChildCount();
        while (childCount != 0) {
            View view = flGdpr.getChildAt(childCount - 1);
            if (view instanceof RelativeLayout) {
                RelativeLayout relativeLayout = (RelativeLayout) view;
                int childCountRl = relativeLayout.getChildCount();
                while (childCountRl != 0) {
                    View findView = relativeLayout.getChildAt(childCountRl - 1);
                    if (findView instanceof Switch) {
                        Switch switchInput  = (Switch) findView;
                        switchInput.getThumbDrawable().setColorFilter(App.getContext().getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
                        switchInput.getTrackDrawable().setColorFilter(App.getContext().getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
                        return;
                    }
                    childCountRl--;
                }
            }
            childCount--;
        }
    }

    private void changeIconGDPRWidget(){
        int childCount = flGdpr.getChildCount();
        while (childCount != 0) {
            View view = flGdpr.getChildAt(childCount - 1);
            if (view instanceof RelativeLayout) {
                RelativeLayout relativeLayout = (RelativeLayout) view;
                int childCountRl = relativeLayout.getChildCount();
                while (childCountRl != 0) {
                    View findView = relativeLayout.getChildAt(childCountRl - 1);
                    if (findView instanceof ImageView) {
                        ImageView imageViewInput  = (ImageView) findView;
                       // imageViewInput.setImageResource(R.drawable.ic_adw);
                        imageViewInput.setVisibility(View.GONE);
                        Utility.setDrawableColor(imageViewInput, App.getContext().getResources().getColor(R.color.green));
                        imageViewInput.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                        int dpValueTop = 16;
                        int dpValueStart = 24;
                        int dpValueBottom = 4;
                        int dpValueEnd = 0;
                        float d = App.getContext().getResources().getDisplayMetrics().density;
                        int marginTop = (int)(dpValueTop * d); // margin in pixels
                        int marginStart = (int)(dpValueStart * d); // margin in pixels
                        int marginEnd = (int)(dpValueEnd * d); // margin in pixels
                        int marginBottom = (int)(dpValueBottom * d); // margin in pixels
                        imageViewInput.setPadding(marginStart,marginTop,marginBottom,marginEnd);
                        return;
                    }
                    childCountRl--;
                }
            }
            childCount--;
        }
    }

    private void changeTextColorGDPRWidget(){
        int childCount = flGdpr.getChildCount();
        while (childCount != 0) {
            View view = flGdpr.getChildAt(childCount - 1);
            if (view instanceof RelativeLayout) {
                RelativeLayout relativeLayout = (RelativeLayout) view;
                int childCountRl = relativeLayout.getChildCount();
                while (childCountRl != 0) {
                    View findView = relativeLayout.getChildAt(childCountRl - 1);
                    if (findView instanceof TextView) {
                        TextView imageViewInput  = (TextView) findView;
                        imageViewInput.setTextColor(getResources().getColor(R.color.backgroundMainTeam));
                        return;
                    }
                    childCountRl--;
                }
            }
            childCount--;
        }
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
