package earn.dollars.fast.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

import butterknife.OnClick;
import butterknife.Unbinder;
import earn.dollars.fast.R;
import earn.dollars.fast.activity.TutorialActivity;

public abstract class BaseTutorialFragment extends Fragment {
    protected Unbinder unbinder;
    protected TutorialActivity activity;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ivArrowBack, R.id.ivArrowForvard})
    protected void onArrowClick(View view) {
        switch (view.getId()) {
            case R.id.ivArrowForvard: activity.goNext(1); break;
            case R.id.ivArrowBack: activity.goNext(-1); break;
        }
    }
}
