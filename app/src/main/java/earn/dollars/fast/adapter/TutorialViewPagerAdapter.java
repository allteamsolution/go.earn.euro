package earn.dollars.fast.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import earn.dollars.fast.activity.TutorialActivity;
import earn.dollars.fast.fragment.FirstTutorialFragment;
import earn.dollars.fast.fragment.SecondTutorialFragment;
import earn.dollars.fast.fragment.ZeroTutorialFragment;

public class TutorialViewPagerAdapter extends FragmentPagerAdapter {

    private final TutorialActivity activity;
    public static final int NUM_PAGES = 3;
    private ZeroTutorialFragment zeroTutorialFragment;
    private FirstTutorialFragment firstTutorialFragment;
    private SecondTutorialFragment secondTutorialFragment;

    public TutorialViewPagerAdapter(FragmentManager fm, TutorialActivity activity) {
        super(fm);
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment result = null;
        switch (position) {
            case 0:
                if (zeroTutorialFragment == null) {
                    zeroTutorialFragment = ZeroTutorialFragment.newInstance(activity);
                }
                result = zeroTutorialFragment; break;
            case 1:
                if (firstTutorialFragment == null) {
                    firstTutorialFragment = FirstTutorialFragment.newInstance(activity);
                }
                result = firstTutorialFragment; break;
            case 2:
                if (secondTutorialFragment == null) {
                    secondTutorialFragment = SecondTutorialFragment.newInstance(activity);
                }
                result = secondTutorialFragment; break;
            default: break;
        }
        return result;
    }
}
