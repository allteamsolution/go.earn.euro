package earn.dollars.fast.rest.body;


import com.google.gson.annotations.SerializedName;

public class GetReferersBody {

    @SerializedName("my_ref_code")
    public String myRefCode;

    public GetReferersBody(String myRefCode) {
        this.myRefCode = myRefCode;
    }
}
