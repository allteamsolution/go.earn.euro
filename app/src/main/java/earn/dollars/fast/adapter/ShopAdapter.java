package earn.dollars.fast.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import earn.dollars.fast.R;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopViewHolder> {

    public interface ShopListener {
        void onShopItem(ShopItem shopItem);
    }

    private List<ShopItem> items;
    private Context context;
    private ShopListener listener;

   //
     public ShopAdapter(List<ShopItem> shopItems, Context ctext, ShopListener listener) {
//   public ShopAdapter(List<ShopItem> shopItems, Context ctext) {
        items = shopItems;
        context = ctext;
        this.listener = listener;
    }

    @Override
    public ShopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_item, parent, false);
        return new ShopViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ShopViewHolder holder, final int position) {
        final ShopItem item = items.get(position);

        holder.greatModel.setText(item.getGreatModel());
        holder.greatPower.setText(item.getGreatPower());
        holder.speedMiner.setText(item.getSpeedMiner()+"");


        holder.btnGreatBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onShopItem(item);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void swapData(List<ShopItem> data){
        items = data;
        notifyDataSetChanged();
    }

    public static class ShopViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.greatModel)
        TextView greatModel;

        @BindView(R.id.speedMiner)
        TextView speedMiner;

        @BindView(R.id.greatPower)
        TextView greatPower;

        @BindView(R.id.btnGreatBuy)
        Button btnGreatBuy;


        ShopViewHolder(View viewItem) {
            super(viewItem);
            ButterKnife.bind(this, viewItem);
        }
    }
}
