package earn.dollars.fast.fragment;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.activity.MainActivity;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.rest.body.ApplyRefCodeBody;
import earn.dollars.fast.rest.response.ApplyRefCodeResponse;
import fx.AwsApp;
import fx.helper.GDPRWidget;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsFragment extends Fragment  {
//    private int currentNumber;
//
@BindView(R.id.flGdpr)
protected FrameLayout flGdpr;


    @BindView(R.id.tvRefCode)
    protected TextView tvRefCode;


    @BindView(R.id.etRefCode)
    protected EditText etRefCode;

    @BindView(R.id.clRefFragment)
    ConstraintLayout clRefFragment;


    @BindView(R.id.button2)
    protected ImageView btnApply;

    @BindView(R.id.copy_ref)
    protected ImageView copy;

    @BindView(R.id.share)
    protected ImageView share;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_settings, container, false);
        ButterKnife.bind(this, view);
        initGDPRWidget();
        recolorSwitch();
        changeIconGDPRWidget();

        init();
        initViews();
        setMyRefCode();
        return view;
    }



    @OnClick(R.id.ll1_site)
    public void onSiteClick() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = AwsApp.getServerConfig().getSiteUrl();
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (Exception ex) {}
    }

    @OnClick(R.id.ll2_privacy)
    public void onPrivacyPolicyClick() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = AwsApp.getServerConfig().getPrivacyPolicyUrl();
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (Exception ex) {}

    }

    @OnClick(R.id.ll3_support)
    public void onSupportFaqClick() {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + Constants.SUPPORT_EMAIL));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Constants.SUPPORT_EMAIL});
            startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_via)));
        }catch (Exception ex) {}
    }


    private void initGDPRWidget() {
        GDPRWidget.create(flGdpr)
                .withDescrTextColor(getResources().getColor(R.color.backgroundMainTeam))
                .withIconColor(getResources().getColor(R.color.green))
                .withTitleTextColor(getResources().getColor(R.color.backgroundMainTeam))
                .withTitleTextSize(16)
                .withDescrTextSize(12)
                .build();
    }


    private void recolorSwitch() {
        int childCount = flGdpr.getChildCount();
        while (childCount != 0) {
            View view = flGdpr.getChildAt(childCount - 1);
            if (view instanceof RelativeLayout) {
                RelativeLayout relativeLayout = (RelativeLayout) view;
                int childCountRl = relativeLayout.getChildCount();
                while (childCountRl != 0) {
                    View findView = relativeLayout.getChildAt(childCountRl - 1);
                    if (findView instanceof Switch) {
                        Switch switchInput  = (Switch) findView;
                        switchInput.getThumbDrawable().setColorFilter(App.getContext().getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                        switchInput.getTrackDrawable().setColorFilter(App.getContext().getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                        return;
                    }
                    childCountRl--;
                }
            }
            childCount--;
        }
    }

    private void changeIconGDPRWidget(){
        int childCount = flGdpr.getChildCount();
        while (childCount != 0) {
            View view = flGdpr.getChildAt(childCount - 1);
            if (view instanceof RelativeLayout) {
                RelativeLayout relativeLayout = (RelativeLayout) view;
                int childCountRl = relativeLayout.getChildCount();
                while (childCountRl != 0) {
                    View findView = relativeLayout.getChildAt(childCountRl - 1);
                    if (findView instanceof ImageView) {
                        ImageView imageViewInput  = (ImageView) findView;
                      //  imageViewInput.setImageResource(R.drawable.ic_adw);
                        imageViewInput.setVisibility(View.GONE);
                        Utility.setDrawableColor(imageViewInput, App.getContext().getResources().getColor(R.color.green));
                        imageViewInput.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                        int dpValueTop = 16;
                        int dpValueStart = 24;
                        int dpValueBottom = 4;
                        int dpValueEnd = 0;
                        float d = App.getContext().getResources().getDisplayMetrics().density;
                        int marginTop = (int)(dpValueTop * d); // margin in pixels
                        int marginStart = (int)(dpValueStart * d); // margin in pixels
                        int marginEnd = (int)(dpValueEnd * d); // margin in pixels
                        int marginBottom = (int)(dpValueBottom * d); // margin in pixels
                        imageViewInput.setPadding(marginStart,marginTop,marginBottom,marginEnd);
                        return;
                    }
                    childCountRl--;
                }
            }
            childCount--;
        }
    }



    @OnClick(R.id.copy_ref)
    public void onCopyRefCode() {
        String ref = tvRefCode.getText().toString();
        JunkProvider.f();

        ClipboardManager manager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData data = ClipData.newPlainText(null, ref.trim());
        manager.setPrimaryClip(data);
        Toast.makeText(getContext(), R.string.toast_copy_successful, Toast.LENGTH_SHORT).show();
    }


    public void setMyRefCode() {
        tvRefCode.setText(App.getCurrentUser().getRefCode());
    }

    private void initViews() {

        // tvRefCode.setText(App.getCurrentUser().getRefCode());
        if (!App.getCurrentUser().getMyReferrer().isEmpty()) {
            etRefCode.setText(App.getCurrentUser().getMyReferrer());
        }

        etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
        etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());

        btnApply.setClickable(!App.getCurrentUser().isRefCodeApplied());
        btnApply.setEnabled(!App.getCurrentUser().isRefCodeApplied());


    }

    @OnClick(R.id.button2)
    protected void onApplyRefCod() {
        if (App.getCurrentUser().getRefCode().isEmpty()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.gen_ref_code), Toast.LENGTH_SHORT).show();
            return;
        } else if (App.getCurrentUser().getRefCode().length() != etRefCode.getText().toString().length()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.ref_code_not_find), Toast.LENGTH_SHORT).show();
            return;
        }else if (App.getCurrentUser().getRefCode().equals(etRefCode.getText().toString())){
            Toast.makeText(getActivity(), getResources().getString(R.string.its_your_ref_code), Toast.LENGTH_SHORT).show();
            return;
        }else{
            App.getCurrentUser().setRefCodeApplied(true);
                    App.getCurrentUser().increaseBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
                    App.getCurrentUser().increaseRefBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
                    App.getCurrentUser().setMyReferrer(etRefCode.getText().toString());
                    App.getCurrentUser().save();

                    etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
                    etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());

                    btnApply.setClickable(!App.getCurrentUser().isRefCodeApplied());
                    btnApply.setEnabled(!App.getCurrentUser().isRefCodeApplied());

            Toast.makeText(App.getContext(), App.getContext().getResources().getString(R.string.ref_code_successful, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency()), Toast.LENGTH_SHORT).show();
        }

//        Call<ApplyRefCodeResponse> call = App.getCraneApi().applyRefCode(new ApplyRefCodeBody(App.getCurrentUser().getRefCode(), etRefCode.getText().toString()));
//
//        call.enqueue(new Callback<ApplyRefCodeResponse>() {
//            @Override
//            public void onResponse(Call<ApplyRefCodeResponse> call, Response<ApplyRefCodeResponse> response) {
//                ApplyRefCodeResponse resp = response.body();
//                if (resp.getResult()) {
//                    App.getCurrentUser().setRefCodeApplied(true);
//                    App.getCurrentUser().increaseBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
//                    App.getCurrentUser().increaseRefBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
//                    App.getCurrentUser().setMyReferrer(etRefCode.getText().toString());
//                    App.getCurrentUser().save();
//
//                    etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
//                    etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());
//
//                    btnApply.setClickable(!App.getCurrentUser().isRefCodeApplied());
//                    btnApply.setEnabled(!App.getCurrentUser().isRefCodeApplied());
//
//                    Toast.makeText(App.getContext(), App.getContext().getResources().getString(R.string.ref_code_successful, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency()), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ApplyRefCodeResponse> call, Throwable t) {
//                Snackbar snackbar = Snackbar
//                        .make(clRefFragment, R.string.error_connect, Snackbar.LENGTH_LONG);
//                snackbar.show();
//            }
//        });
    }
    private void init() {
        initViews();
        Utility.setDrawableColor(copy,getResources().getColor(R.color.colorPrimary));
        Utility.setDrawableColor(share,getResources().getColor(R.color.colorPrimary));

    }

    @OnClick(R.id.share)
    public void onShareClicked(){
        Utility.share(getContext(),true);
    }
}

