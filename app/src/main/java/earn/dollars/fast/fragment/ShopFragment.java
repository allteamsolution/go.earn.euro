package earn.dollars.fast.fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.adapter.ShopAdapter;
import earn.dollars.fast.adapter.ShopItem;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.Utility;

public class ShopFragment extends Fragment  {

    public ShopAdapter adapter;

    @BindView(R.id.recViewShop)
    protected RecyclerView recListWeatherView;

    private RecyclerView.LayoutManager ll;

    @BindView(R.id.tvBalanceShop)
    protected TextView tvBalanceShop;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shop_fragment, container, false);
        ButterKnife.bind(this, view);

        ll = new LinearLayoutManager(getContext());
        recListWeatherView.setLayoutManager(ll);
        createShopItems();
        updateBalance();
        return view;
    }

    private void updateBalance() {
        double curentBalance = App.getCurrentUser().getBalance();
        tvBalanceShop.setText(Utility.formatBalance(curentBalance) + " " + Constants.CRYPT_CODE);
    }

    private void createShopItems() {
        List<ShopItem> shopItemsList = new ArrayList<>();
        shopItemsList.add(new ShopItem(getString(R.string.model_asic_one), Constants.MINER_SPEED_ONE+ Constants.CRYPT_CODE,Constants.MINER_POWER_ONE,1));
        shopItemsList.add(new ShopItem(getString(R.string.model_asic_two), Constants.MINER_SPEED_TWO+ Constants.CRYPT_CODE,Constants.MINER_POWER_TWO,2));
        shopItemsList.add(new ShopItem(getString(R.string.model_asic_three), Constants.MINER_SPEED_THREE+ Constants.CRYPT_CODE,Constants.MINER_POWER_THREE,3));
        shopItemsList.add(new ShopItem(getString(R.string.model_asic_four), Constants.MINER_SPEED_FOUR + Constants.CRYPT_CODE,Constants.MINER_POWER_FOUR,4));
        shopItemsList.add(new ShopItem(getString(R.string.model_asic_five), Constants.MINER_SPEED_FIVE+ Constants.CRYPT_CODE,Constants.MINER_POWER_FIVE,5));
       // shopItemsList.add(new ShopItem(getString(R.string.free_slot), Constants.MINER_SPEED_ZERO,Constants.MINER_POWER_ZERO,0));


        adapter = new ShopAdapter(shopItemsList, getActivity(),shopItem -> {
            showBuyDialog(shopItem);
        });
        recListWeatherView.setAdapter(adapter);

        tvBalanceShop.setText(App.getCurrentUser().getBalance()+"");
    }


    public void showBuyDialog(ShopItem shopItem){

       tvBalanceShop.setText(shopItem.getId()+"");


    }

}

