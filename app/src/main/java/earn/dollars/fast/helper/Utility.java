package earn.dollars.fast.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;

import java.util.Calendar;
import java.util.Locale;

import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;


public class Utility {



    public static final String INTENT_TYPE_TEXT_PLAIN = "text/plain";

    static public void setDrawableColor(Drawable drawable, int color) {
        drawable.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    static public void setDrawableColor(ImageView iv, int color) {
        iv.getDrawable().mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    static public void setBackgroundColor(View iv, int color) {
        iv.getBackground().mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }




    public static boolean hasInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) return false;
        return activeNetwork.isConnectedOrConnecting();
    }

    public static void share(Context context, boolean withRef){
        JunkProvider.f();

        Intent intent = new Intent(Intent.ACTION_SEND);

        String text = "";
        String text0 = context.getResources().getString(R.string.share_text, App.getContext().getPackageName());
        String text1 = context.getResources().getString(R.string.share_text_middle, App.getCurrentUser().getRefCode());
        String text2 = context.getResources().getString(R.string.share_text_end);

        if (withRef) {
            text = text0 + text1 + text2;
        } else {
            text = text0 + text2;
        }
        intent.setType(INTENT_TYPE_TEXT_PLAIN);
        intent.putExtra(Intent.EXTRA_TEXT, text);

        String shareTitle = App.getContext().getResources().getString(R.string.share_chooser_title);
        if (withRef){
            shareTitle = App.getContext().getResources().getString(R.string.share_chooser_title_with_ref);
        }
        Intent chooser = Intent.createChooser(intent, shareTitle);

        if (intent.resolveActivity(App.getContext().getPackageManager()) != null) {
            context.startActivity(chooser);
        }
    }

    public static long getTime(){
        return System.currentTimeMillis();
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yy", cal).toString();
        return date;
    }

    public static String formatBalance(double balance){
        String formattedString = String.format(Constants.FORMAT_BALANCE, balance);
        return formattedString;
    }

    public static String formatBalance(float balance, int points){
        String formattedString = String.format(Constants.FORMAT_BALANCE, balance);
        return formattedString;
    }


//    public static Drawable getCheckBoxDrawable(){
//        return MagicDrawable.createCheckBox(App.getContext().getResources().getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp),
//                App.getContext().getResources().getColor(R.color.white),
//                App.getContext().getResources().getDrawable(R.drawable.ic_check_circle_black_24dp),
//                App.getContext().getResources().getColor(R.color.fabcolor));
//    }

}
