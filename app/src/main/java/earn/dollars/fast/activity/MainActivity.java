package earn.dollars.fast.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Random;

import javax.xml.transform.Templates;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.BuildConfig;
import earn.dollars.fast.R;
import earn.dollars.fast.control.InAppNotificationControl;
import earn.dollars.fast.dialog.GreatMinerBuyDialogHelper;
import earn.dollars.fast.event.MineStartedEvent;
import earn.dollars.fast.event.MineStoppedEvent;
import earn.dollars.fast.event.MinedEvent;
import earn.dollars.fast.fragment.MainFragment;
import earn.dollars.fast.fragment.NewsFragment;
import earn.dollars.fast.fragment.PayoutFragment;
import earn.dollars.fast.fragment.ReferralFragment;
import earn.dollars.fast.fragment.SettingsFragment;
import earn.dollars.fast.fragment.ShopFragment;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.MyRateDialogHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.model.User;
import earn.dollars.fast.receiver.NetworkStateReceiver;
import earn.dollars.fast.rest.body.GenerateRefCodeBody;
import earn.dollars.fast.rest.body.GetReferersBody;
import earn.dollars.fast.rest.response.GenerateRefCodeResponse;
import earn.dollars.fast.rest.response.GetReferersResponse;
import earn.dollars.fast.service.MiningForegroundService;
import fx.AwsApp;
import fx.helper.AwsAppPreferenceManager;
import fx.helper.GDPRHelper;
import fx.helper.LocalHelper;
import fx.helper.finalmigration.FinalMigrationHelper;
import fx.helper.loco.DeviceIdHelper;
import fx.helper.loco.LocoMotif;
import fx.helper.migration.CraneMigrationHelper;
import fx.helper.news.NewsFeedCallback;
import great.utils.UIHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NewsFeedCallback,  NetworkStateReceiver.NetworkStateReceiverListener{


//    @BindView(R.id.ivAnimated)
//    protected ImageView ivAnimated;

//    @BindView(R.id.tvBalance)
//    protected TextView balance;

//    @BindView(R.id.llTasks)
//    protected LinearLayout llTasks;

    @BindView(R.id.bottom_navigation)
    protected BottomNavigationView bottomNavigation;

//    @BindView(R.id.maningImageView)
//    protected ImageView tvStartStop;

    @BindView(R.id.llNotif)
    protected LinearLayout llNotif;

//    @BindView(R.id.btnOffers)
//    protected ImageView btnOffers;

    private InAppNotificationControl inAppNotificationControl;
    private boolean shownNotif;
    private long timeSpendSec;
    private Random random = new Random();

    private MenuItem itemOffers;
    private MenuItem itemSettings;
    private MenuItem itemShare;




    private Runnable inAppRunnable = new Runnable() {
        @Override
        public void run() {
            if (inAppNotificationControl != null) {
                showNotif();
                shownNotif = true;
            }

            delayNotif();
            JunkProvider.f();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        getSupportActionBar().setElevation(0);
        if (!App.getCurrentUser().isTutorialFinish()) {
            Intent intent = new Intent(this, TutorialActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        GDPRHelper.initConsentAndPrivacyDialog(this, getResources().getColor(R.color.colorPrimaryDark), true);
        CraneMigrationHelper.builder(this)
                .withBonuses("300", "satoshi")
                .withRewardCallback(new CraneMigrationHelper.MigrationRewardCallback() {
                    @Override
                    public void onReward() {
                        App.getCurrentUser().increaseBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
                        //    updateBalance();
                    }
                }).show();

        FinalMigrationHelper.builder(this).show();
       // showMenu();
        initBottomNavigation();
        if (App.getCurrentUser().getRefCode().isEmpty()) {
            registerRefCode();
        }

        if (!App.getCurrentUser().getRefCode().isEmpty())
            getReferrers();

        if (!App.getCurrentUser().isDemoPremiumUsed()) {
            MyRateDialogHelper.show(this);
        }

        updateBalance();
        inAppNotificationControl = new InAppNotificationControl(llNotif);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainFragment()).commit();
        }
        //checkOffersTime();
       // inAppNotificationControl.show();

    }



    private void initBottomNavigation() {
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                UIHelper.hideKeyboard(MainActivity.this);
                int id = item.getItemId();

                switch (id) {
                    case R.id.action_main:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainFragment()).commit();
//                        currentFragment = mainViewPagerAdapter.getMainFragment();
//                        mainFragment = (MainFragment)currentFragment;
                        setTitle(getString(R.string.app_name));
                        updateBalance();
                        break;
                    case R.id.action_settings:

                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new SettingsFragment()).commit();
//
                        setTitle(getString(R.string.settings_title));
                        break;
                    case R.id.action_payout:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new PayoutFragment()).commit();
//                        smileGameFragment = (SmileGameFragment)currentFragment;
                        setTitle(getString(R.string.payout));
                        break;
                    case R.id.action_news:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new NewsFragment()).commit();
//                        smileGameFragment = (SmileGameFragment)currentFragment;
                        setTitle(getString(R.string.news));
                        break;

                }
                return true;
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MinedEvent event) {
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStartedEvent event) {
       // initStartBtn(true);
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStoppedEvent event) {
        //initStartBtn(false);
        updateBalance();
    }

//    @OnClick(R.id.llNews)
//    public void newsClicked(){
//        Intent intent = new Intent(this ,NewsFeedActivity.class);
//        startActivity(intent);
//    }
//
//
//    @OnClick(R.id.llWallet)
//    public void walletClicked(){
//        Intent intent = new Intent(this ,PayoutActivity.class);
//        startActivity(intent);
//    }
//
//
//    @OnClick(R.id.llTasks)
//    protected void onTasksClick() {
//        LocoMotif controller = LocoMotif.create(LocoMotif.Builder.create()
//                .setCurrencyName(CurrencyHelper.getLocoCurrency(App.getCurrency()))
//                .setWithOffers(true)
//                .setColorAcent(Color.parseColor("#88004D"))
//                .setColorAcentPressed(Color.parseColor("#88004D"))
//                .setRewardMultiplier(CurrencyHelper.getLocoRewardMultiplier(App.getCurrency()))
//                .setFormat(CurrencyHelper.getLocoFormat(App.getCurrency()))
//                .setLocoRewardCallback(new LocoMotif.LocoRewardCallback() {
//                    @Override
//                    public float getMyBalance() {
//                        return CurrencyHelper.getLocoCurrentBalance(App.getCurrency());
//                    }
//
//                    @Override
//                    public void onTotalReward(float totalReward, float delta) {
//                        User user = App.getCurrentUser();
//                        user.increaseBalance(CurrencyHelper.getLocoTotalReward(App.getCurrency(), delta));
//                        App.setCurrentUser(user);
//                    }
//                }));
//
//        controller.open(this);
//    }



    @Override
    public void onNewsItemClicked(String url) {
        //TODO Open in your browser clergy.pencil.activity (NewsItemActivity)
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (android.content.ActivityNotFoundException ex) {}
    }
    @Override
    public Drawable getNewsItemPlaceholder() {
        return getResources().getDrawable(R.drawable.ic_back);
    }
    @Override
    public Point getNewsPlaceholderImageSizeInDp() {
        return new Point(140, 100);
    }

    @Override
    public int getNewsItemLayoutId() {
        return R.layout.news_item;
    }


//    public void showMenu(){
//        if (isShownOffers()){
//            llTasks.setVisibility(View.VISIBLE);
//            btnOffers.setVisibility(View.VISIBLE);
//        } else {
//            llTasks.setVisibility(View.GONE);
//            btnOffers.setVisibility(View.GONE);
//        }
//    }


    @Override
    public int getCustomAdItemLayoutId() {
        return 0;
    }

    @Override
    public boolean showAdsInAdapter() {
        return false;
    }


    @Override
    public String getFeedUrl() {
        String urlString;
        if (LocalHelper.isRuRegion()) {
            urlString = "https://lenta-novostei.com/feed/";
        } else {
            urlString = "https://crypto-news.space/en/" + "feed/";
        }

        return urlString;
    }



    private void registerRefCode() {
        String deviceId = DeviceIdHelper.id();
        String packageName = App.getContext().getPackageName();
        Call<GenerateRefCodeResponse> call = App.getCraneApi().registerRefCode(new GenerateRefCodeBody(packageName, deviceId));

        call.enqueue(new Callback<GenerateRefCodeResponse>() {
            @Override
            public void onResponse(Call<GenerateRefCodeResponse> call, Response<GenerateRefCodeResponse> response) {
                GenerateRefCodeResponse resp = response.body();
                if (!resp.getResult().isEmpty()) {
                    App.getCurrentUser().setRefCode(resp.getResult());
                    App.getCurrentUser().save();
                }
            }

            @Override
            public void onFailure(Call<GenerateRefCodeResponse> call, Throwable t) {

            }
        });
    }

    private void getReferrers() {
        String refCode = App.getCurrentUser().getRefCode();
        Call<GetReferersResponse> call = App.getCraneApi().getReferrers(new GetReferersBody(refCode));

        call.enqueue(new Callback<GetReferersResponse>() {
            @Override
            public void onResponse(Call<GetReferersResponse> call, Response<GetReferersResponse> response) {
                GetReferersResponse resp = response.body();
                Log.d("LOG", Integer.toString(resp.getResult()));
                if (resp.getResult() > App.getCurrentUser().getMyReferrersCount()) {
                    double reward = CurrencyHelper.getRefCodeRewardPerReferer(App.getCurrency()) * (resp.getResult() - App.getCurrentUser().getMyReferrersCount());
                    App.getCurrentUser().increaseRefBalance(reward);
                    App.getCurrentUser().increaseBalance(reward);
                    App.getCurrentUser().setMyReferrersCount(resp.getResult());
                    App.getCurrentUser().save();
                    updateBalance();
                }
            }

            @Override
            public void onFailure(Call<GetReferersResponse> call, Throwable t) {
                Log.d("LOG", t.toString());
            }
        });
    }

    protected void showNotif() {
        if (AwsApp.getServerConfig().customPropery1.equals("1") || BuildConfig.DEBUG) {
            inAppNotificationControl.show();
        }
    }

    private void delayNotif() {
        int rand = 10000 + random.nextInt(Constants.INAPP_TIMEOUT_BIG);
        if (!shownNotif) {
            rand = 10000 + random.nextInt(Constants.INAPP_TIMEOUT_SMALL);
        }            JunkProvider.f();

        Log.d("TIMESK", "time : " + rand);
        App.getUIHandler().postDelayed(inAppRunnable, +rand);
    }

    private void updateBalance(){

        double curentBalance = App.getCurrentUser().getBalance();
    //    balance.setText(Utility.formatBalance(curentBalance) + " " + Constants.CRYPT_CODE);
    }


    @Override
    protected void onResume() {
        super.onResume();

        timeSpendSec = System.currentTimeMillis() / 1000;
        int num = 30;
        if (this instanceof MainActivity){
            num = 10;
        }
        if (random.nextInt(num) % num == 0){
            showNotif();
            shownNotif = true;
        }

        App.getUIHandler().removeCallbacks(inAppRunnable);
        delayNotif();
        updateBalance();
    }

//    private void initStartBtn(boolean isServiceRunning) {
//        if (isServiceRunning) {
//            tvStartStop.setImageResource(R.drawable.pause);
//            updateBalance();
//           // ivAnimated.setImageResource(R.drawable.miner_dr);
//           // ((AnimationDrawable) ivAnimated.getDrawable()).start();
//        } else {
//           // ivAnimated.setImageResource(R.drawable.main_bg);
//            tvStartStop.setImageResource(R.drawable.play);
//            updateBalance();
//            //((AnimationDrawable) ivAnimated.getDrawable()).stop();
//        }
//
//        updateBalance();
//    }






//    @OnClick(R.id.maningImageView)
//    public void onivStartStopClicked(){
//        if (mService != null){
//            if (mService.isRunning()){
//                Intent service = MiningForegroundService.createIntent(MiningForegroundService.ACTION_STOP_MINING);
//                App.getContext().startService(service);
//            } else {
//                Log.d("JJJJJJJJ", "save time : " + Utility.getTime());
//                App.getCurrentUser().timeLastRestart = Utility.getTime();
//                App.setCurrentUser(App.getCurrentUser());
//                Intent service = MiningForegroundService.createIntent(MiningForegroundService.ACTION_START_MINING);
//                App.getContext().startService(service);
//            }
//        } else {
//        }
//    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.action_bar_menu, menu);
//        itemOffers = menu.findItem(R.id.action_offers);
//        itemShare = menu.findItem(R.id.action_share);
//        itemSettings = menu.findItem(R.id.action_settings);
//
//        checkOffersTime();
//
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            Intent intent = new Intent(this, SettingsActivity.class);
//            startActivity(intent);
//        }
//
//        if (id == R.id.action_share) {
//            Utility.share(this, true);
//        }
//
//        if (id == R.id.action_offers) {
//            LocoMotif controller = LocoMotif.create(LocoMotif.Builder.create()
//                    .setCurrencyName(CurrencyHelper.getLocoCurrency(App.getCurrency()))
//                    .setWithOffers(true)
//                    .setColorAcent(Color.parseColor("#f65922"))
//                    .setColorAcentPressed(Color.parseColor("#f65922"))
//                    .setRewardMultiplier(CurrencyHelper.getLocoRewardMultiplier(App.getCurrency()))
//                    .setFormat(CurrencyHelper.getLocoFormat(App.getCurrency()))
//                    .setLocoRewardCallback(new LocoMotif.LocoRewardCallback() {
//                        @Override
//                        public float getMyBalance() {
//                            return CurrencyHelper.getLocoCurrentBalance(App.getCurrency());
//                        }
//
//                        @Override
//                        public void onTotalReward(float totalReward, float delta) {
//                            User user = App.getCurrentUser();
//                            user.increaseBalance(CurrencyHelper.getLocoTotalReward(App.getCurrency(), delta));
//                            App.setCurrentUser(user);
//                        }
//                    }));
//
//            controller.open(this);
//        }
//        return super.onOptionsItemSelected(item);
//
//
//    }







}

