package earn.dollars.fast.helper;


import earn.dollars.fast.BuildConfig;

public class Constants {

    public static final String MARKET_URL_PREFIX = "market://details?id=";
    public static final String MARKET_BROWSER_URL_PREFIX = "https://play.google.com/store/apps/details?id=";
    public static final String PROMO_CODE_5000 = "get-bitcoin";
    public static final String PROMO_CODE_500 = "bonusfree";
    public static final String PACK = "clientapp.swiftcom.org";

    public static final String BTC = "BTC";
    public static final String EUR = "EUR";
    public static final String USD = "USD";
    public static final String RUB = "RUB";
    public static final int NOTIFICATION_CODE = 33;

    public final static String CRYPT_CODE = " EUR";

    public static final float DEF_PLAN_REW = 0.0000021f;
    public static final float PREMIUM_PLAN_REW = 0.0000042f;
    public static final float PLATINUM_PLAN_REW = 0.0000063f;

    public static final long DEMO_TIME = 20 * 60 * 60 * 1000;



    public static final float MINING_REWARD = 0.000002f;
    //public static final float MINING_REWARD = 0.00005f;
    public static final String MINER_SPEED_ZERO = "0.00000004";
    public static final String MINER_SPEED_ONE = "0.00000004";
    public static final String MINER_SPEED_TWO = "0.00000012";
    public static final String MINER_SPEED_THREE = "0.00000024";
    public static final String MINER_SPEED_FOUR = "0.0000004";
    public static final String MINER_SPEED_FIVE = "0.00000064";

    public static final float MINER_SPEED_ZERO_FLOAT = 0.00000004f;
    public static final float MINER_SPEED_ONE_FLOAT = 0.00000004f;
    public static final float MINER_SPEED_TWO_FLOAT = 0.00000012f;
    public static final float MINER_SPEED_THREE_FLOAT = 0.00000024f;
    public static final float MINER_SPEED_FOUR_FLOAT = 0.0000004f;
    public static final float MINER_SPEED_FIVE_FLOAT = 0.00000064f;

    public static final String MINER_COST_ZERO = "0.00000004";
    public static final String MINER_COST_ONE = "0.01728";
    public static final String MINER_COST_TWO = "0.03456";
    public static final String MINER_COST_THREE = "0.06912";
    public static final String MINER_COST_FOUR = "0.13824";
    public static final String MINER_COST_FIVE = "0.27648";

    public static final float MINER_COST_ZERO_FLOAT = 0.00000004f;
    public static final float MINER_COST_ONE_FLOAT = 0.01728f;
    public static final float MINER_COST_TWO_FLOAT = 0.03456f;
    public static final float MINER_COST_THREE_FLOAT = 0.06912f;
    public static final float MINER_COST_FOUR_FLOAT = 0.13824f;
    public static final float MINER_COST_FIVE_FLOAT = 0.27648f;

    public static final String MINER_POWER_ZERO = "0.00\nGH/s";
    public static final String MINER_POWER_ONE = "0.05\nGH/s";
    public static final String MINER_POWER_TWO = "0.10\nGH/s";
    public static final String MINER_POWER_THREE = "0.15\nGH/s" ;
    public static final String MINER_POWER_FOUR = "0.20\nGH/s";
    public static final String MINER_POWER_FIVE = "0.25\nGH/s";

    public static final String MINER_POWER_ZERO_MAIN = "0.00 GH/s";
    public static final String MINER_POWER_ONE_MAIN  = "0.05 GH/s";
    public static final String MINER_POWER_TWO_MAIN  = "0.10 GH/s";
    public static final String MINER_POWER_THREE_MAIN  = "0.15 GH/s";
    public static final String MINER_POWER_FOUR_MAIN  = "0.20 GH/s";
    public static final String MINER_POWER_FIVE_MAIN  = "0.25 GH/s";





    public static double minPayout = 20.0;

    public static final String CRYPTO = "EUR";
    public static final String CRYPTO_SAT = "satoshi";

    public static final String FLASHLIGHT_PACKAGE = "great.apps.volume.flash";

    public static final String FORMAT_BALANCE = "%.7f";

    public static final double MIN_PAYOUT = 0.005;
    public static final double MIN_PAYOUT_PART = 500000;

    public static final double btcToSatoshi =  MIN_PAYOUT_PART / MIN_PAYOUT;
    public static final double satoshiToBtc =  MIN_PAYOUT / MIN_PAYOUT_PART;

    public static final String SUPPORT_EMAIL = "berenyierika68@gmail.com";

    public static final String IS_FIRST_LAUNCH = "is_first_launch";
    public static final String IS_TUTORIAL = "is_tutorial";

    public static final short NORMAL = 0;
    public static final short FAST = 1;


    public static final int MIN_DURATION_NORMAL = 48;
    public static final int MAX_DURATION_NORMAL = 96;

    public static final int MIN_DURATION_FAST = 24;
    public static final int MAX_DURATION_FAST = 36;


    public static final int AMAZING_NUMBER = 1000;
    public static final int HIGH_NUMBER = 999;
    public static final int MEDIUM_HIGH_NUMBER = 900;
    public static final int MEDIUM_LOW_NUMBER = 400;
    public static final int LOW_NUMBER = 200;
    public static int TIME_AD_REWARDS;
    public static int INAPP_TIMEOUT_BIG;
    public static int INAPP_TIMEOUT_SMALL;
    public static int VIDEO_REWARD_TIME ;
    public static int ANIMATION_COOLDOWN = 5 ;

    public static double minPayoutUSD = 20;
    public static double minPayoutBtc = 0.05;//5 000 000 μETH
    public static double minPayoutSatoshi = minPayoutBtc * 100 * 1000 * 1000;//500 kSatoshi

    static {
        if (BuildConfig.DEBUG) {
            TIME_AD_REWARDS = 15 * 1000; // 15sec
            INAPP_TIMEOUT_BIG = 1500;
            INAPP_TIMEOUT_SMALL = 100;
        } else {
            TIME_AD_REWARDS = 15 * 60 * 1000;// 15 min
            INAPP_TIMEOUT_BIG = 150000;
            INAPP_TIMEOUT_SMALL = 10000;
        }
    }

    static {
        if(BuildConfig.DEBUG){
            VIDEO_REWARD_TIME = 10*1000;
        }else{
            VIDEO_REWARD_TIME = 15*60*1000;
        }
    }

    public static double satoshiToDollarCoef = minPayoutUSD /minPayoutSatoshi;
    public static double dollarToSatoshiCoef = minPayoutSatoshi/ minPayoutUSD;

    public static double btcToDollarCoef = minPayoutUSD /minPayoutBtc;
    public static double dollarToBtcCoef = minPayoutBtc/ minPayoutUSD;
}
