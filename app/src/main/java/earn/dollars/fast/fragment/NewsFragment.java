package earn.dollars.fast.fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import earn.dollars.fast.R;
import fx.helper.Utility;
import fx.helper.news.NewsFeedFragment;

public class NewsFragment extends Fragment {



    private NewsFeedFragment newsFeedFragment;

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        addNewsFragment();
        return view;
    }

    private void addNewsFragment() {
        newsFeedFragment = NewsFeedFragment.newInstance();

        getActivity().getFragmentManager()
                .beginTransaction()
                .add(R.id.flNews, newsFeedFragment)
                .commitAllowingStateLoss();
    }

}
