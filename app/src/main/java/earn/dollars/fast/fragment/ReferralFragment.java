package earn.dollars.fast.fragment;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.helper.CurrencyHelper;
import earn.dollars.fast.helper.Utility;
import earn.dollars.fast.rest.body.ApplyRefCodeBody;
import earn.dollars.fast.rest.response.ApplyRefCodeResponse;
import great.utils.UIHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReferralFragment extends Fragment  {
//    private int currentNumber;
//
@BindView(R.id.tvRefCode)
protected TextView tvRefCode;

    @BindView(R.id.tvRefCodeDescription)
    protected TextView tvRefCodeDescription;

    @BindView(R.id.etRefCode)
    protected EditText etRefCode;

    @BindView(R.id.clRefFragment)
    ConstraintLayout clRefFragment;


    @BindView(R.id.button2)
    protected TextView btnApply;

    @BindView(R.id.copy_ref)
    protected ImageView copy;

    //    @BindView(R.id.getMoneyTextViewOne)
//    protected TextView tvGetMoneyOne;
//
//    @BindView(R.id.getPrizeBtnOne)
//    protected ImageView ivGetMoneyOne;
//
//    @BindView(R.id.getMoneyTextViewTwo)
//    protected TextView tvGetMoneyTwo;
//
//    @BindView(R.id.getPrizeBtnTwo)
//    protected ImageView ivGetMoneyTwo;
//
//    @BindView(R.id.balance)
//    protected TextView tvBalance;
//
//    @BindView(R.id.coinOne)
//    protected ImageView ivCoinOne;
//
//    @BindView(R.id.coinTwo)
//    protected ImageView ivCoinTwo;
//
//    @BindView(R.id.percentOfWallet)
//    protected TextView tvPercent;
//
//    @BindView(R.id.llNotif)
//    protected LinearLayout llNotif;
//
//    private BaseFragment.OnUpdateBalanceCallback onUpdateBalanceCallback;
//    private MainFragment.MainFragmentCallback mainFragmentCallback;
//
//
//    private InAppNotificationControl inAppNotificationControl;
//    private boolean shownNotif;
//    private long timeSpendSec;
//    private Random random = new Random();
//
//    private Runnable inAppRunnable = new Runnable() {
//        @Override
//        public void run() {
//            if (inAppNotificationControl != null) {
//                showNotif();
//                shownNotif = true;
//            }
//
//            delayNotif();
//            JunkProvider.f();
//        }
//    };
//
//
//    public static MainFragment newInstance(MainActivity activity) {
//        MainFragment fragment = new MainFragment();
////        fragment.onUpdateBalanceCallback = activity;
////        fragment.mainFragmentCallback = activity;
//        return fragment;
//    }
//
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_referral, container, false);
        ButterKnife.bind(this, view);
        ///updateBalance();
//        inAppNotificationControl = new InAppNotificationControl(llNotif);
//        initPayout();
        init();
        initViews();
        setMyRefCode();
        return view;
    }


    @OnClick(R.id.copy_ref)
    public void onCopyRefCode() {
        String ref = tvRefCode.getText().toString();
        JunkProvider.f();

        ClipboardManager manager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData data = ClipData.newPlainText(null, ref.trim());
        manager.setPrimaryClip(data);
        Toast.makeText(getContext(), R.string.toast_copy_successful, Toast.LENGTH_SHORT).show();
    }


        public void setMyRefCode() {
        tvRefCode.setText(App.getCurrentUser().getRefCode());
    }

    private void initViews() {

        // tvRefCode.setText(App.getCurrentUser().getRefCode());
        if (!App.getCurrentUser().getMyReferrer().isEmpty()) {
            etRefCode.setText(App.getCurrentUser().getMyReferrer());
        }

        etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
        etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());

        btnApply.setClickable(!App.getCurrentUser().isRefCodeApplied());
        btnApply.setEnabled(!App.getCurrentUser().isRefCodeApplied());

        tvRefCodeDescription.setText(getResources()
                .getString(R.string.enter_ref_code_descr, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency())
        );
    }

    @OnClick(R.id.button2)
    protected void onApplyRefCod() {
        if (App.getCurrentUser().getRefCode().isEmpty()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.gen_ref_code), Toast.LENGTH_SHORT).show();
            return;
        } else if (App.getCurrentUser().getRefCode().length() != etRefCode.getText().toString().length()) {
            Toast.makeText(getActivity(), getResources().getString(R.string.ref_code_not_find), Toast.LENGTH_SHORT).show();
            return;
        }
        Call<ApplyRefCodeResponse> call = App.getCraneApi().applyRefCode(new ApplyRefCodeBody(App.getCurrentUser().getRefCode(), etRefCode.getText().toString()));

        call.enqueue(new Callback<ApplyRefCodeResponse>() {
            @Override
            public void onResponse(Call<ApplyRefCodeResponse> call, Response<ApplyRefCodeResponse> response) {
                ApplyRefCodeResponse resp = response.body();
                if (resp.getResult()) {
                    App.getCurrentUser().setRefCodeApplied(true);
                    App.getCurrentUser().increaseBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
                    App.getCurrentUser().increaseRefBalance(CurrencyHelper.getApplyRefCodeReward(App.getCurrency()));
                    App.getCurrentUser().setMyReferrer(etRefCode.getText().toString());
                    App.getCurrentUser().save();

                    etRefCode.setClickable(!App.getCurrentUser().isRefCodeApplied());
                    etRefCode.setEnabled(!App.getCurrentUser().isRefCodeApplied());

                    btnApply.setClickable(!App.getCurrentUser().isRefCodeApplied());
                    btnApply.setEnabled(!App.getCurrentUser().isRefCodeApplied());

                    Toast.makeText(App.getContext(), App.getContext().getResources().getString(R.string.ref_code_successful, String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), CurrencyHelper.getApplyRefCodeReward(App.getCurrency()) ) + App.getCurrency()), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ApplyRefCodeResponse> call, Throwable t) {
                Snackbar snackbar = Snackbar
                        .make(clRefFragment, R.string.error_connect, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
    }
    private void init() {
        initViews();
        Utility.setDrawableColor(copy,getResources().getColor(R.color.green));
    }






    public void updateBalance() {
        initViews();
    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        timeSpendSec = System.currentTimeMillis() / 1000;
//        int num = 30;
//        if (this instanceof MainFragment) {
//            num = 10;
//        }
//        if (random.nextInt(num) % num == 0) {
//            showNotif();
//            shownNotif = true;
//        }
//
//        App.getUIHandler().removeCallbacks(inAppRunnable);
//        delayNotif();
//        App.getUIHandler().post(timerRunnableOne);
//        App.getUIHandler().post(timerRunnableTwo);
//        updateBalance();
//    }
//
//    public void updateBalance() {
//        tvBalance.setText(String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), App.getCurrentUser().getBalance()));
//        initPayout();
//        //    tvReferalBalance.setText(String.format(CurrencyHelper.getFormatBalance(App.getCurrency()), App.getCurrentUser().getRefBalance())+ " " + App.getCurrency());
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        App.getUIHandler().removeCallbacks(timerRunnableOne);
//        App.getUIHandler().removeCallbacks(timerRunnableTwo);
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//    }
//
//    @Override
//    public void onWithdrawHistoryClicked() {
//
//    }
//
//    public interface MainFragmentCallback {
//        public void onTakeReward(int number);
//    }
//
//    @OnClick(R.id.getPrizeBtnOne)
//    public void onbtnOneClick() {
//        long timeLastWatched = System.currentTimeMillis();
//        User user = App.getCurrentUser();
//        user.setTimeToWatchVideoOne(timeLastWatched);
//        App.setCurrentUser(user);
//        App.getCurrentUser().save();
//        Intent intent = new Intent(getActivity(), RewardActivity.class);
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.getPrizeBtnTwo)
//    public void onbtnTwoClick() {
//        long timeLastWatched = System.currentTimeMillis();
//        User user = App.getCurrentUser();
//        user.setTimeToWatchVideoTwo(timeLastWatched);
//        App.setCurrentUser(user);
//        App.getCurrentUser().save();
//        Intent intent = new Intent(getActivity(), RewardActivity.class);
//        startActivity(intent);
//    }
//
//    @OnClick(R.id.ivPayout)
//    public void onPayoutClick() {
//        if (App.getCurrentUser().getBalance() > 20.0) {
//            double balance = App.getCurrentUser().getBalance();
//            App.getCurrentUser().increaseBalance(-balance);
//            App.getCurrentUser().save();
//            updateBalance();
//        }
//        PayoutDialogHelper.show(getActivity());
//    }
//
//    private Runnable timerRunnableOne = new Runnable() {
//        @Override
//        public void run() {
//
//            long timeLastWatchingOne = App.getCurrentUser().getTimeToWatchVideoOne();
//            int cooldown = Constants.VIDEO_REWARD_TIME;
//            long timeToNewVideoOne = (System.currentTimeMillis() - (timeLastWatchingOne + cooldown)) / 1000;
//            long leftTime = ((timeLastWatchingOne + cooldown) - System.currentTimeMillis()) / 1000;
//            if (leftTime > 0) {
//                tvGetMoneyOne.setText(convert(Math.abs(timeToNewVideoOne)));
//                App.getUIHandler().postDelayed(timerRunnableOne, 1000);
//                ivGetMoneyOne.setClickable(false);
//                ivGetMoneyOne.setAlpha(0.5f);
//                ivCoinOne.setVisibility(View.GONE);
//
//            } else {
//                tvGetMoneyOne.setText(getString(R.string.get_reward));
//                ivCoinOne.setVisibility(View.VISIBLE);
//                App.getUIHandler().removeCallbacks(timerRunnableOne);
//                ivGetMoneyOne.setClickable(true);
//                ivGetMoneyOne.setAlpha(1.0f);
//            }
//        }
//    };
//
//    private Runnable timerRunnableTwo = new Runnable() {
//        @Override
//        public void run() {
//
//            long timeLastWatchingTwo = App.getCurrentUser().getTimeToWatchVideoTwo();
//            int cooldown = Constants.VIDEO_REWARD_TIME;
//            long timeToNewVideoTwo = (System.currentTimeMillis() - (timeLastWatchingTwo + cooldown)) / 1000;
//            long leftTime = ((timeLastWatchingTwo + cooldown) - System.currentTimeMillis()) / 1000;
//            if (leftTime > 0) {
//                tvGetMoneyTwo.setText(convert(Math.abs(timeToNewVideoTwo)));
//                App.getUIHandler().postDelayed(timerRunnableTwo, 1000);
//                ivGetMoneyTwo.setClickable(false);
//                ivGetMoneyTwo.setAlpha(0.5f);
//                ivCoinTwo.setVisibility(View.GONE);
//
//            } else {
//                tvGetMoneyTwo.setText(getString(R.string.get_reward));
//                ivCoinTwo.setVisibility(View.VISIBLE);
//                App.getUIHandler().removeCallbacks(timerRunnableTwo);
//                ivGetMoneyTwo.setClickable(true);
//                ivGetMoneyTwo.setAlpha(1.0f);
//            }
//        }
//    };
//
//    public static String convert(long seconds) {
//        String result = "";
//        if (seconds < 60) {
//            result = seconds + " " + App.getContext().getResources().getString(R.string.sec);
//        } else if (seconds > 60 && seconds < 3600) {
//            result = (int) Math.floor(seconds / 60) + " " + App.getContext().getResources().getString(R.string.min);
//        }
//
//        return result;
//    }
//
//    public void initPayout() {
//        int percent = (int) (100 * (App.getCurrentUser().getBalance() / Constants.minPayout));
//        if (percent > 100) {
//            percent = 100;
//        }
//        tvPercent.setText(percent + " %");
//    }
//
//
//    protected void showNotif() {
//        if (AwsApp.getServerConfig().customPropery1.equals("1") || BuildConfig.DEBUG) {
//            inAppNotificationControl.show();
//        }
//    }
//
//    private void delayNotif() {
//        int rand = 10000 + random.nextInt(Constants.INAPP_TIMEOUT_BIG);
//        if (!shownNotif) {
//            rand = 10000 + random.nextInt(Constants.INAPP_TIMEOUT_SMALL);
//        }
//        JunkProvider.f();
//
//        Log.d("TIMESK", "time : " + rand);
//        App.getUIHandler().postDelayed(inAppRunnable, +rand);
//    }

}

