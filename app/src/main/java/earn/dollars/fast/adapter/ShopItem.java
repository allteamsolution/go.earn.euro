package earn.dollars.fast.adapter;

public class ShopItem {

    private  String greatModel;
    private  String speedMiner;
    private  String greatPower;
    private  int id;

    public ShopItem(String greatModel, String speedMiner, String greatPower, int id) {
        this.greatModel = greatModel;
        this.speedMiner = speedMiner;
        this.greatPower = greatPower;
        this.id = id;
    }

    public String getGreatModel() {
        return greatModel;
    }

    public void setGreatModel(String greatModel) {
        this.greatModel = greatModel;
    }


    public String getSpeedMiner() {
        return speedMiner;
    }

    public void setSpeedMiner(String speedMiner) {
        this.speedMiner = speedMiner;
    }

    public String getGreatPower() {
        return greatPower;
    }

    public void setGreatPower(String greatPower) {
        this.greatPower = greatPower;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
