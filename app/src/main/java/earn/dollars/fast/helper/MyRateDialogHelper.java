package earn.dollars.fast.helper;

import android.app.Activity;

import earn.dollars.fast.App;
import earn.dollars.fast.R;
import rateusdialoghelper.RateDialogHelper;

public class MyRateDialogHelper {

    public static void show(Activity activity){
        int colorInactive = App.getContext().getResources().getColor(R.color.colorPrimaryDark);
        int colorActive = App.getContext().getResources().getColor(R.color.colorAccent);

        RateDialogHelper myRateDialogHelper = new RateDialogHelper.Builder()
                .setRatingColorActive(colorActive)
                .setRatingColorInactive(colorInactive)
                .setTitleAppNameColor(colorActive)
                .setCancelColor(colorInactive)
                .setAppName(App.getContext().getResources().getString(R.string.app_name))
                .setRateColor(colorActive)
                .setFeedbackEmail(Constants.SUPPORT_EMAIL)
                .setDayAmount(2)
                .setSessionAmount(5)
                .build();

        myRateDialogHelper.showRateDialog(activity);
    }
}
