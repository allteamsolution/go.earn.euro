package earn.dollars.fast.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import earn.dollars.fast.App;
import earn.dollars.fast.R;
import earn.dollars.fast.helper.Constants;
import earn.dollars.fast.helper.ShopHelper;
import earn.dollars.fast.model.User;

public class GreatMinerBuyDialogHelper extends BaseDialogHelper{


    @BindView(R.id.tvTitle)
    public TextView tvTitle;

    @BindView(R.id.tvDescr)
    public TextView tvDescr;

    @BindView(R.id.powerMinerDialog)
    public TextView powerMinerDialog;

    @BindView(R.id.tvModel)
    public TextView tvModel;


    @BindView(R.id.btnCancel)
    public TextView btnCancel;

    @BindView(R.id.btnOk)
    public TextView btnOk;

//    private BuyGreatMainerDialogHelperCallback callback;
//
//    public interface BuyGreatMainerDialogHelperCallback {
//        void onMinerBuyClick();
//    }


    private GreatMinerBuyDialogHelperClickListener listener;

    private int modelID;
    private int mainerID;



    public interface GreatMinerBuyDialogHelperClickListener {
        void onBuyGreatMinerClicked(int amount);
    }

    public static void show(Context context, GreatMinerBuyDialogHelperClickListener listener, int modelID, int mainerID ){
        new GreatMinerBuyDialogHelper(context, listener, modelID, mainerID);
    }

    private GreatMinerBuyDialogHelper(Context context, GreatMinerBuyDialogHelperClickListener listener, int modelID,int mainerID){
        super(context);
        this.listener = listener;
        this.modelID = modelID;
        this.mainerID = mainerID;
//        callback = (BuyGreatMainerDialogHelperCallback) context;

        init();
    }

    @Override
    protected View onCreateView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.buy_dialog, null);
        ButterKnife.bind(this, view);




        return view;
    }

    private void init(){

        if(modelID == 0){
            tvDescr.setText(getContext().getResources().getString(R.string.speed_up) +  " " + Constants.MINER_POWER_ONE_MAIN + " " +
                    getContext().getResources().getString(R.string.forr) + " " + Constants.MINER_COST_ONE + " " + Constants.CRYPT_CODE);
            tvModel.setText(getContext().getResources().getString(R.string.model_one));
            powerMinerDialog.setText(Constants.MINER_POWER_ONE_MAIN);
        }else if(modelID==1){
            tvDescr.setText(getContext().getResources().getString(R.string.speed_up) +  " " + Constants.MINER_POWER_TWO_MAIN + " " +
                    getContext().getResources().getString(R.string.forr) + " " + Constants.MINER_COST_TWO + " " + Constants.CRYPT_CODE);
            tvModel.setText(getContext().getResources().getString(R.string.model_two));
            powerMinerDialog.setText(Constants.MINER_POWER_TWO_MAIN);
        }else if(modelID==2){
            tvDescr.setText(getContext().getResources().getString(R.string.speed_up) +  " " + Constants.MINER_POWER_THREE_MAIN + " " +
                    getContext().getResources().getString(R.string.forr) + " " + Constants.MINER_COST_THREE + " " + Constants.CRYPT_CODE);
            tvModel.setText(getContext().getResources().getString(R.string.model_three));
            powerMinerDialog.setText(Constants.MINER_POWER_THREE_MAIN);
        }else if (modelID ==3){
            tvDescr.setText(getContext().getResources().getString(R.string.speed_up) +  " " + Constants.MINER_POWER_FOUR_MAIN + " " +
                    getContext().getResources().getString(R.string.forr) + " " + Constants.MINER_COST_FOUR + " " + Constants.CRYPT_CODE);
            tvModel.setText(getContext().getResources().getString(R.string.model_four));
            powerMinerDialog.setText(Constants.MINER_POWER_FOUR_MAIN);
        }else if ( modelID ==4){
            tvDescr.setText(getContext().getResources().getString(R.string.speed_up) +  " " + Constants.MINER_POWER_FIVE_MAIN + " " +
                    getContext().getResources().getString(R.string.forr) + " " + Constants.MINER_COST_FIVE + " " + Constants.CRYPT_CODE);
            tvModel.setText(getContext().getResources().getString(R.string.model_five));
            powerMinerDialog.setText(Constants.MINER_POWER_FIVE_MAIN);
        }else {

        }
//        double price = RewardManager.getConstReward();
//
//        price1 = price;
//        price5 = 5f * price * 0.95;
//        price10 = 10f * price * 0.90;
//
//        tvDescrBuy1.setText(getContext().getResources().getString(R.string.buy_1_smile_coin, price));
//        tvDescrBuy5.setText(getContext().getResources().getString(R.string.buy_5_smile_coin, price5));
//        tvDescrBuy10.setText(getContext().getResources().getString(R.string.buy_10_smile_coin, price10));
//
//        User user = App.getCurrentUser();
//
//        runAnimation(user);
    }




    @OnClick(R.id.btnOk)
    public void onBtnOkayClicked(){
        double balance = App.getCurrentUser().getBalance();
        double cost = ShopHelper.getGreatMinerCost(modelID);
        double currentBalance = balance - cost;
        if (currentBalance > 0) {
            User user = App.getCurrentUser();
           user.increaseBalance(-cost);
           if(mainerID ==1){
               user.setGreatMinerIdOne(modelID+1);
           }else if(mainerID ==2){
               user.setGreatMinerIdTwo(modelID+1);
           }else if(mainerID ==3 ){
               user.setGreatMinerIdThree(modelID+1);
           }else if(mainerID ==4){
               user.setGreatMinerIdFour(modelID+1);
           }else if(mainerID==5){
               user.setGreatMinerIdFive(modelID+1);
           }else if (mainerID==6){
               user.setGreatMinerIdSix(modelID+1);
           }
                App.setCurrentUser(user);
                App.getCurrentUser().save();
                App.MINING_BONUS = App.MINING_BONUS + ShopHelper.getGreatMinerSpeed(modelID);

            dismiss();


        }else{
            Toast toast = Toast.makeText(getContext(),
                    getContext().getResources().getString(R.string.no_money), Toast.LENGTH_SHORT);
            toast.show();
        }


//        if(App.getCurrentUser().getBalance()-price1 > 0) {
//            onMinerBuyClick(1, price1);
//        }else {
//            Toast toast = Toast.makeText(getContext(),
//                    App.getContext().getResources().getString(R.string.no_balance) +  App.getContext().getResources().getString(R.string.no_smile_coins_descr), Toast.LENGTH_SHORT);
//            toast.show();
//        }
    }

    @OnClick(R.id.btnCancel)
    public void onBtnCancelClick(){
        dismiss();
//        if(App.getCurrentUser().getBalance()-price5 > 0) {
//            onMinerBuyClick(5, price5);
//        }else {
//            Toast toast = Toast.makeText(getContext(),
//                    App.getContext().getResources().getString(R.string.no_balance) +  App.getContext().getResources().getString(R.string.no_smile_coins_descr), Toast.LENGTH_SHORT);
//            toast.show();
//        }
    }



    private void onMinerBuyClick(int id){
        if (listener != null) {
            listener.onBuyGreatMinerClicked(id);
        }

        dismiss();
    }

}